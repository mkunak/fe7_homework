let list = document.createElement('ul');
$('.content').append(list);

class Hero {
    constructor(name, gender, starships, homeWorld, id) {
        this.name = name,
            this.gender = gender,
            this.starships = starships,
            this.homeWorld = homeWorld,
            this.id = id
    }

    render() {
        let item = document.createElement('li');
        item.innerHTML = `<p>${this.name}</p><p>${this.gender}</p><p>${this.homeWorld}</p>`;
        $(list).append(item);
        if (this.starships.length) {
            item.innerHTML += `<button id=${this.id}>Get Starships</button>`;
            $(`#${this.id}`).on('click', this.getShips.bind(this));
        }
    }

    getShips() {
        let element = document.getElementById(`${this.id}`).parentElement;
        this.starships.forEach(ship => {
            Hero.getInfoFromAjax(ship, function (dataStarship) {
                element.innerHTML += `<p class="starship__title">${dataStarship.name}</p>`;
            })
        })
    }

    static getInfoFromAjax(url, callback) {
        $.ajax({
            url: url,
            success: callback
        });
    }
}

// $(function () {
Hero.getInfoFromAjax('https://swapi.co/api/people/', function (data) {
    console.log(data.results);
    data.results.forEach(function (item, index) {
        Hero.getInfoFromAjax(item.homeworld, function (dataHomeworld) {
            let person = new Hero(item.name, item.gender, item.starships, dataHomeworld.name, index);
            person.render();
        })
    })
});
// });

//////////////////////////////////////////////////////////////////////////////////////////////////////
// function getInfoFromAjax(url, callback) {
//     $.ajax({
//         url: url,
//         success: callback
//     });
// }
//
// function render(parent, el, elemText, index, elemClassName) {
//     parent.append(function (i) {
//         if (i === index) {
//             return `<${el} class=${elemClassName}>${elemText}</${el}>`;
//         }
//     });
// }
//
// getInfoFromAjax('https://swapi.co/api/people/', function (data) {
//     let _this = null;
//     $('.content').append('<ol class="characters-list"></ol>'); // console.log(data.results);
//     data.results.forEach(function (item, index) {
//         const person = {
//             name: item.name,
//             gender: item.gender,
//             getHomeworld: function () {
//                 getInfoFromAjax(item.homeworld, function (dataHomeworld) {
//                     this.homeworld = dataHomeworld.name;
//                     render($('.item'), 'p', `planet: ${this.homeworld}`, index, 'item__description');
//                     if (item.starships.length) {
//                         render($('.item'), 'button', 'Starships list', index, 'button');
//                         $('button').on("click", function (event) {
//                             event.preventDefault();
//                             $(event.target).closest('.item').append(function () {
//                                 _this = this;
//                                 if ($(_this).index() === index) {
//                                     item.starships.forEach(function (starshipUrl) {
//                                         getInfoFromAjax(starshipUrl, function (dataStarship) {
//                                             $(_this).append(`<p class="starship__title">${dataStarship.name}</p>`);
//                                             $('.starship__title').on('click', function (event) {
//                                                 event.preventDefault();
//                                                 if ($('.starship__parameters').length) {
//                                                     $('.starship__parameters').remove();
//                                                 }
//                                                 _this = this;
//                                                 getInfoFromAjax(starshipUrl, function (dataStarship) {
//                                                     if ($(_this).closest('.item').index() === index && $(_this).text() === dataStarship.name) {
//                                                         $(_this).after(`<p class="starship__parameters">manufacturer: ${dataStarship.manufacturer}</p>`);
//                                                         $(_this).after(`<p class="starship__parameters">passengers: ${dataStarship.passengers}</p>`);
//                                                         $(_this).after(`<p class="starship__parameters">class: ${dataStarship.starship_class}</p>`);
//                                                         $(_this).after(`<p class="starship__parameters">model: ${dataStarship.model}</p>`);
//                                                     }
//                                                 });
//                                             });
//                                         });
//                                     });
//                                 }
//                             });
//                             $(this).replaceWith(function () {
//                                 return `<h3 class="button">Manned spaceships:</h3>`;
//                             });
//
//                         });
//                     }
//                 });
//             }
//         };
//
//         $('.characters-list').append(() => `<li class="item"></li>`);
//         if (person.gender === 'n/a') {
//             person.gender = 'droid';
//         }
//         render($('.item'), 'p', person.name, index, 'item__title');
//         render($('.item'), 'p', `gender: ${person.gender}`, index, 'item__description');
//         person.getHomeworld();
//     })
// });