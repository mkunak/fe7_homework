class Planet {
  constructor(planet) {
    this.name = planet.name,
      this.climate = planet.climate,
      this.terrain = planet.terrain,
      this.residents = planet.residents,
      this.films = planet.films
  }

  renderPlanetName() {
    list.innerHTML += `<li class="item"><h2 class="item__title">${this.name}</h2></li>`;
  }

  renderPlanetClimate(index) {
    document.querySelectorAll('.item').forEach((item, itemIndex) => {
      if (itemIndex === index) {
        item.innerHTML += `<h3 class="item__heading">Climate:</h3><p class="item__description">${this.climate}</p>`;
      }
    });
  }

  renderPlanetTerrain(index) {
    document.querySelectorAll('.item').forEach((item, itemIndex) => {
      if (itemIndex === index) {
            item.innerHTML += `<h3 class="item__heading">Terrain:</h3>
                               <p class="item__description">${this.terrain}</p>`;
      }
      // insertElemsAfter ?
    });
  }

  renderPlanetResidents(index) {
    if (this.residents.length) {
      Planet.insertElemsAfter(document.querySelectorAll('.item__title'), index, 'h3',
        'item__heading', 'Residents:');

      let requests = [];
      this.residents.forEach(url => requests.push(Planet.getPromiseFromAjax(url)));

      Promise.all(requests)
        .then(residents => residents.forEach(resident =>
          Planet.insertElemsAfter(document.querySelectorAll('.item'), index, 'p',
            'item__description', resident.name, 0)));
    }
  }

  renderPlanetFilms(index) {
    if (this.residents.length) {
      Planet.insertElemsAfter(document.querySelectorAll('.item__title'), index, 'h3',
        'item__heading', 'Films:');

      let requests = [];
      this.films.forEach(url => requests.push(Planet.getPromiseFromAjax(url)));

      Promise.all(requests)
        .then(films => films.forEach(film =>
          Planet.insertElemsAfter(document.querySelectorAll('.item'), index, 'p',
            'item__description', film.title, 1)));
    }
  }

  static insertElemsAfter(parents, index, tagName, className, content, parentIndex) {
    parents.forEach((item, itemIndex) => {
      if (itemIndex === index) {
        isNaN(parentIndex) ? Planet.insertElemAfter(item, tagName, className, content) :
          Planet.insertElemAfter(item.querySelectorAll('.item__heading')[parentIndex], tagName, className, content);
      }
    });
  }

  static insertElemAfter(parent, tagName, className, content) {
    let elem = document.createElement(tagName);
    elem.className = className;
    elem.textContent = content;
    parent.after(elem);
  }

  static getPromiseFromAjax(url) {
    return new Promise(function (resolve, reject) {
      const xhr = new XMLHttpRequest();
      xhr.open("GET", url);
      xhr.send();
      xhr.addEventListener('load', function () {
        if (xhr.status !== 200) {
          reject(`${xhr.status}, ${xhr.statusText}`);
        } else {
          resolve(JSON.parse(xhr.response));
        }
      });
    });
  }
}

const content = document.querySelector('.content');
const list = document.createElement('ul');
content.append(list);

Planet.getPromiseFromAjax('https://swapi.co/api/planets/')
  .then((response) => {
    let planetArr = [];
    response.results.forEach((item, index) => {
      console.log(item);
      let planet = new Planet(item);
      planet.renderPlanetName();
      planet.renderPlanetClimate(index);
      planet.renderPlanetTerrain(index);
      planetArr.push(planet);
    });
    return planetArr;
  })
  .then(planetArr => {
    planetArr.forEach((planet, index) => {
      planet.renderPlanetFilms(index);
      planet.renderPlanetResidents(index);
    });
  })
  .catch((reject) => console.log(reject));