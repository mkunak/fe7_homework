const button = document.getElementById('button');

button.addEventListener('click', async () => {
  const response1 = await fetch('https://api.ipify.org/?format=json');
  const ip = await response1.json(); // console.log(ip);
  const lang = 'ru';
  const fields = ['continent', 'country', 'region', 'regionName', 'city', 'district']; // continent,country,region,city,district
  const response2 = await fetch(`http://ip-api.com/json/${ip.ip}?lang=${lang}&fields=${fields}`); // 'http://ip-api.com/json/?fields=61439'
  const data = await response2.json(); // console.log(data);

  const list = document.createElement('ul');
  list.className = 'list';
  if (document.querySelector('.list')) {
    document.querySelector('.list').remove();
  }
  for (let key in data) {
    list.innerHTML += `<li class="item__description"><span class="item__key">${key}: </span>${data[key]}</li>`;
  }
  button.after(list);
});