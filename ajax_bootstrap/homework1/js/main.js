function insertParagraph(content, className, parentElem) {
    let paragraph = document.createElement('p');
    paragraph.textContent = content;
    paragraph.className = className;
    parentElem.appendChild(paragraph);
}

const xhr = new XMLHttpRequest();
let episodesArray = [];
let charactersArray = [];
const content = document.querySelector('.content');
xhr.open("GET", "https://swapi.co/api/films/");
xhr.send();
xhr.onload = function () {
    if (xhr.status !== 200) {
        console.log(`${xhr.status}, ${xhr.statusText}`);
    } else {
        episodesArray = JSON.parse(xhr.response).results.sort(function (a, b) {
            return a.episode_id - b.episode_id;
        });
        const ol = document.createElement('ol');
        for (let i = 0; i < episodesArray.length; i++) {
            // Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl). // console.log(`Episode ID: ${episodesArray[i].episode_id}.\nEpisode title: ${episodesArray[i].title}.\nOpening crawl: ${episodesArray[i].opening_crawl}.`);
            let li = document.createElement('li');
            li.className = 'item';
            insertParagraph(`Episode #${episodesArray[i].episode_id}`, 'item__episode', li);
            insertParagraph(`${episodesArray[i].title}`, 'item__title', li);
            insertParagraph(`${episodesArray[i].opening_crawl}`, 'item__description', li);
            //Под этими данными вывести кнопку с надписью Список персонажей.
            const button = document.createElement('button');
            button.textContent = `Characters list of Episode #${episodesArray[i].episode_id}`;
            button.className = 'button';
            li.appendChild(button);
            ol.appendChild(li);
            // При клике на кнопку Список персонажей должен отправлятся AJAX-запрос на сервер для поисках всех
            // персонажей саги, которые появлялись в этом фильме. Список персонажей можно получить из свойства
            // characters каждого фильма.
            button.addEventListener('click', function (event) {
                event.preventDefault();
                button.style.display = 'none';
                if (document.querySelector('.characters-list')) {
                    document.querySelector('.characters-list').remove();
                }
                // Пока загружаются персонажи фильма, прокручивать под кнопкой Список персонажей анимацию загрузки.
                // Анимацию можно использовать любую.
                let newDiv = document.createElement('div');
                newDiv.innerHTML = '<div class="lds-ripple"><div></div><div></div></div>';
                button.after(newDiv);
                episodesArray[i].characters.forEach(function (item) {
                    charactersArray = [];
                    let xhr = new XMLHttpRequest();
                    xhr.open('GET', item);
                    xhr.send();
                    xhr.onload = function () {
                        if (xhr.status !== 200) {
                            console.log(`${xhr.status}, ${xhr.statusText}`);
                        } else {
                            charactersArray.push(JSON.parse(xhr.response));
                            if (document.querySelector('.characters-list')) {
                                document.querySelector('.characters-list').remove();
                            }
                            // Как только с сервера будет получена информация о всех персонажах какого-либо фильма,
                            // вывести эту информацию на страницу под кнопкой Список персонажей для этого фильма, а
                            // саму кнопку можно удалить.
                            newDiv = document.createElement('div');
                            newDiv.className = 'characters-list';
                            const ol = document.createElement('ol');
                            if (episodesArray[i].characters.length === charactersArray.length) {
                                insertParagraph('Characters list (names only):', 'item__title', ol);
                                charactersArray.forEach(function (item) {
                                    const li = document.createElement('li');
                                    insertParagraph(item.name, 'names', li);
                                    ol.appendChild(li);
                                });
                                document.querySelector('.lds-ripple').remove();
                                button.style.display = 'block';
                            }
                            newDiv.appendChild(ol);
                            button.after(newDiv);
                        }
                    };
                });
            });
        }
        content.appendChild(ol);
    }
};