// all 'PATH' module methods: https://nodejs.org/dist/latest-v10.x/docs/api/path.html

const path = require('path');

// console.log(path);
// console.log(__filename);
// console.log(path.basename(__filename));
// console.log(path.dirname(__filename));
// console.log(path.extname(__filename));
// console.log(path.parse(__filename).base);

console.log(path.join(__dirname, 'test', 'index.html'));
console.log(path.resolve(__dirname, './test', '/index.html'));

