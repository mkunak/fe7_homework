const fs = require('fs'); // fs = File System
const path = require('path');

const dirName = 'notes';

// fs.mkdir = makes new Directory
// fs.mkdir(path.join(__dirname, dirName), err => {
//   if (err) throw new Error(err);
//
//   console.log(`The directory ${dirName} was created.`);
// });

// fs.writeFile - makes new File
// fs.writeFile(
//   path.join(__dirname, dirName, 'myNote.txt'),
//   'Hello! This my first draft note. Buy.',
//   (err) => {
//     if (err) throw new Error(err);
//
//     console.log(`The file myNote.txt was created.`);
//   }
// );

// fs.appendFile - add new info to the end of existing file
// fs.appendFile(
//   path.join(__dirname, dirName, 'myNote.txt'),
//   'Info from appendFile',
//   (err) => {
//     if (err) throw new Error(err);
//
//     console.log(`The file myNote.txt was changed.`);
//   }
// );

// fs.readFile - read files
// fs.readFile(
//   path.join(__dirname, dirName, 'myNote.txt'),
//   'utf-8',
//   (err, data) => {
//     if (err) throw new Error(err);
//     console.log(data);
//   }
// );

fs.rename(
  path.join(__dirname, dirName, 'myNote.txt'),
  path.join(__dirname, dirName, 'myNotes.txt'),
  (err) => {
    if (err) throw new Error(err);
    console.log('file has been renamed');
  }
);