// мы сами должны создать и настроить определенные сервера, чтобы они работали. Для этого служит модуль HTTP.
const http = require("http");

const server = http.createServer((req, res) => {
  console.log(req.url);

  res.write("<h1>Hello from nodeJS</h1>");
  res.write("<h2>Hello from nodeJS</h2>");
  res.write("<h3>Hello from nodeJS</h3>");
  res.end(`
  <div style="width: 250px; height: 100px; background: red; border: 1px solid grey;
  display: flex; justify-content:center; align-items:center">
    <p>This is square/rectangular</p>
  </div>
  `);
});

server.listen(3000, () => {
  console.log("Server is on");
});
