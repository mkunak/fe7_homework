console.log('My directory has path: ', __dirname);
console.log('My file has path: ', __filename);

user = {
  name: 'Mykola',
  familyName: 'Kunak',
  age: 33
};

module.exports = {
  user,
  sayHello() {
    console.log(`user ${user.name} ${user.familyName} say "Hello everyone"`);
  }};
