// мы сами должны создать и настроить определенные сервера, чтобы они работали. Для этого служит модуль HTTP.
const http = require("http");
const path = require("path");
const fs = require("fs");

const server = http.createServer((req, res) => {
  // http.createServer(req, res) - Returns a new instance of the http.Server class.
  console.log(req.method);
  if (req.method === "GET") {
    res.writeHead(200, {
      "Content-Type": "text/html; charset=utf-8"
    });

    if (req.url === "/") {
      fs.readFile(
        path.join(__dirname, "views", "index.html"),
        "utf-8",
        (err, content) => {
          if (err) {
            throw err;
          }
          res.end(content);
        }
      );
    } else if (req.url === "/about") {
      fs.readFile(
        path.join(__dirname, "views", "about.html"),
        "utf-8",
        (err, content) => {
          if (err) {
            throw err;
          }
          res.end(content);
        }
      );
    } else if (req.url === "/api/users") {
      res.writeHead(200, {
        "Content-Type": "text/json"
      });

      const users = [
        {
          name: "Mykola",
          age: 25
        },
        {
          name: "Helena",
          age: 18
        }
      ];

      res.end(JSON.stringify(users));
    }
  } else if (req.method === "POST") {
    res.writeHead(200, {
      "Content-Type": "text/html; charset=utf-8"
    });

    const messageBody = [];

    req.on("data", data => {
      messageBody.push(Buffer.from(data));
    });

    req.on("end", () => {
      const message = messageBody.toString().split("=")[1];
      console.log(message);
      res.end(`
      <h1>Your message/Ваше сообщение: ${message}</h1>
    `);
    });
  }
});

server.listen(3000, () => {
  console.log("Server is on..");
});
