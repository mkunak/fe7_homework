const express = require("express");
const path = require("path");
const mongoose = require("mongoose");
const exphbs = require("express-handlebars");

const homeRoutes = require("./routes/home");
const coursesRoutes = require("./routes/courses");
const addRoutes = require("./routes/add");
const ordersRoutes = require("./routes/orders");
const cartRoutes = require("./routes/cart");
const authRoutes = require("./routes/auth");

const User = require("./models/user");

const app = express(); // это аналог объекта Сервер

const hbs = exphbs.create({
  defaultLayout: "main",
  extname: "hbs"
});

app.engine("hbs", hbs.engine); // регистрируем в express, что есть такой движок hbs со зачением hbs.engine. Этот движок будет заниматься рендерингом html-страниц.
app.set("view engine", "hbs"); // после этого с помощью метода set мы его начинаем использовать. Здесь первый аргумент-строка - это ключевое слово.
app.set("views", "views"); // здесь мы указываем папку, где будут храниться все наши view-шаблоны.
//После этого переименуем расширение файла index.html -> index.hbs

const userId = "5db98ccbbb5e2521a417d3c3";
app.use(async (req, res, next) => {
  try {
    const user = await User.findById(userId);
    // console.log("'req.user' at 28 in basic-types.js: ", user);
    req.user = user;
    next(); // функция next() вызывается здесь, чтобы продолжились выполняться другие use().
  } catch (err) {
    console.log("'err' at 35 in basic-types.js: ", err);
  }
});

// метод use() позволяет добавлять в наше приложение дополнительные middleware, т.е. функциональность:
app.use(express.static(path.join(__dirname, "public")));
app.use(express.urlencoded({ extended: true }));

// регистрируем роуты:
app.use("/", homeRoutes);
app.use("/courses", coursesRoutes);
app.use("/add", addRoutes);
app.use("/cart", cartRoutes);
app.use("/orders", ordersRoutes);
app.use("/auth", authRoutes);

const PORT = process.env.PORT || 3000;

async function start() {
  try {
    const url =
      "mongodb+srv://mkunak:y6ft8R84RCbdxGHG@cluster0-nmb6a.mongodb.net/shop";
    await mongoose.connect(url, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useFindAndModify: false
    });

    const candidate = await User.findOne();
    
    if (!candidate) {
      const user = new User({
        email: "mail@fakemail.com",
        name: "John",
        cart: { items: [] }
      });
      await user.save(); // сохраняем локальную переменную user.
    }

    app.listen(PORT, () => {
      console.log(`server is running on port ${PORT} ...`);
    });
  } catch (error) {
    console.log(error);
  }
}

start();
