const formatCurrency = price => {
  return new Intl.NumberFormat("ua-UA", {
    currency: "hrn",
    style: "currency"
  }).format(price);
};

const formatDate = date => {
  return new Intl.DateTimeFormat('ua-UA', {
    year: 'numeric',
    month: 'long',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }).format(new Date(date))
}

document.querySelectorAll(".price").forEach(node => {
  node.textContent = formatCurrency(node.textContent);
});

document.querySelectorAll(".date").forEach(node => {
  node.textContent = formatDate(node.textContent);
});

const $cart = document.getElementById("cart");

if ($cart) {
  $cart.addEventListener("click", event => {
    if (event.target.classList.contains("js-remove")) {
      const id = event.target.dataset.id;
      console.log(id);

      fetch("/cart/remove/" + id, { method: "delete" })
        .then(res => res.json())
        .then(cart => {
          console.log(cart);
          if (cart.courses.length) {
            const html = cart.courses
              .map(item => {
                return `
                <tr>
                  <td>${item.title}</td>
                  <td>${item.price}</td>
                  <td>${item.count}</td>
                  <td>
                    <button class="btn btn-small js-remove" data-id="${item.id}">Delete</button>
                  </td>
                </tr>
              `;
              })
              .join("");
            $cart.querySelector("tbody").innerHTML = html;
            $cart.querySelector(".price").textContent = formatCurrency(
              cart.price
            );
          } else {
            $cart.innerHTML = `<p>Cart is empty</p>`;
          }
        })
        .catch();
    }
  });
}

M.Tabs.init(document.querySelectorAll('.tabs'));