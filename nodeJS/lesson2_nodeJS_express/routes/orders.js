// импортируем модель заказа:
const Order = require("../models/order");

// формируем роутер:
const { Router } = require("express");
const router = Router();

// для начала нам необходимо отрендерить страницу:
router.get("/", async (req, res) => {
  try {
    const orders = await Order.find({
      "user.userId": req.user._id
    }).populate("user.userId"); // получаем список всех заказов согласно id пользователя
    res.render("orders", {
      isOrder: true,
      title: "Orders",
      orders: orders.map(order => {
        return {
          ...order._doc,
          price: order.courses.reduce((total, course) => {
            return (total += course.course.price * course.count);
          }, 0)
        };
      })
    });
  } catch (err) {
    console.log(err);
  }
});

// следующим этапом обрабатываем POST-запрос на данную страницу orders:
router.post("/", async (req, res) => {
  try {
    //для начала получим все что есть в корзине:
    const user = await req.user.populate("cart.items.courseId").execPopulate();
    console.log(user.cart.items);

    // теперь формируем человеческий формат объекта с курсами:
    const courses = user.cart.items.map(item => ({
      count: item.count,
      course: { ...item.courseId._doc }
    }));

    // после объекта курсов формируем объект заказа:
    const order = new Order({
      user: {
        name: req.user.name,
        userId: req.user
      },
      courses: courses
    });

    await order.save(); // ждем пока создастся новый заказ
    // но! после создания заказа нужно почистить корзину:
    await req.user.clearCart();

    res.redirect("/orders"); // переход на страницу с указанным роутом
  } catch (err) {
    console.log(err);
  }
}); // сразу после написания этой обработки у нас есть проблема, т.к. нет данных, поэтому нужно создать модель

module.exports = router;
