const { Router } = require("express");
const Course = require("../models/course");
const router = Router();

// Получаем список всех курсов из БД. Здесь указываем урл, который мы обрабатываем:
router.get("/", async (req, res) => {
  const courses = await Course.find()
    .populate("userId", "email name")
    .select("price title img");
  // .catch(err => {
  //   console.log(err);
  // }); // после подключения библиотеки mongoose, вместо кастомного метода getAll() пишем find(), который выводит все данные из БД
  console.log(courses);
  res.render("courses", {
    title: "Courses",
    isCourses: true,
    courses
  });
});

router.get("/:id/edit", async (req, res) => {
  if (!req.query.allow) {
    return res.redirect("/");
  }

  // вместо кастомного метода getById() пишем стандартный метод из библиотеки mongoose findById()
  const course = await Course.findById(req.params.id);
  //   .catch(err => {
  //   console.log(err);
  // });

  res.render("./course-edit", {
    title: "Edit course",
    course
  });
});

// delete course
router.post("/remove", async (req, res) => {
  try {
    await Course.deleteOne({ _id: req.body.id });
    res.redirect("/courses");
  } catch (error) {
    console.log(error);
  }
});

// редактируем отдельный курс
router.post("/edit", async (req, res) => {
  const { id } = req.body;
  delete req.body.id;
  await Course.findByIdAndUpdate(id, req.body);
  res.redirect("/courses");
});

// получаем отдельный курс
router.get("/:id", async (req, res) => {
  console.log("ID at 57 in courses.js: ", req.params.id);
  const course = await Course.findById(req.params.id);
  //   .catch(err => {
  //   console.log(err);
  // });
  res.render("course", {
    layout: "empty",
    title: "Course details",
    course
  });
});

module.exports = router;
