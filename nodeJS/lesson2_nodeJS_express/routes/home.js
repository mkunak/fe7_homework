const {Router } = require('express');
const router = Router();

// здесь указываем урл, который мы обрабатываем:
router.get("/", (req, res) => {
  res.status(200);
  // res.sendFile(path.join(__dirname, "views", "index.html")); // это команда обработки статической html-страницы
  res.render("index", {
    title: "Main",
    isHome: true
  });
});

module.exports = router;