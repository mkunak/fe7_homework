const { Router } = require("express");
const Course = require("../models/course");
const router = Router();

function mapCartItems(cart) {
  return cart.items.map(item => ({
    ...item.courseId._doc,
    id: item.courseId.id,
    count: item.count
  }));
}

function calcPrice(courses) {
  return courses.reduce((total, item) => (total += item.price * item.count), 0);
}

router.post("/add", async (req, res) => {
  const course = await Course.findById(req.body.id);
  await req.user.addToCart(course);
  res.redirect("/cart");
});

router.delete("/remove/:id", async (req, res) => {
  await req.user.removeFromCart(req.params.id); // id берем динамически из адресной строки, чтобы это сделать - используем req.params.id
  // теперь на фронт нужно вернуть актуальный объект корзины
  const user = await req.user.populate("cart.items.courseId").execPopulate();
  const courses = mapCartItems(user.cart);
  const cart = {
    courses,
    price: calcPrice(courses)
  };

  res.status(200).json(cart);
});

router.get("/", async (req, res) => {
  const user = await req.user.populate("cart.items.courseId").execPopulate(); // задача - получить корзину, а она находится в модели user

  const courses = mapCartItems(user.cart); // console.log("*** cart.js: 29:", courses);

  res.render("cart", {
    title: "Cart",
    isCart: true,
    courses: courses,
    price: calcPrice(courses)
  });
});

module.exports = router;
