const { Router } = require("express");
const Course = require("../models/course");
const router = Router();

// здесь указываем урл, который мы обрабатываем и рендерим страницу:
router.get("/", (req, res) => {
  res.status(200);
  // res.sendFile(path.join(__dirname, "views", "about.html"));
  res.render("add", {
    title: "Add",
    isAdd: true
  });
});

// добавление нового курса. добавляем роут с методом post(), чтобы обработать запрос формы:
router.post("/", async (req, res) => {
  const course = new Course({
    title: req.body.title,
    price: req.body.price,
    img: req.body.img,
    // userId: req.user._id,
    userId: req.user
  });

  try {
    await course.save(); // сохраняем курс в МонгоДБ
    res.redirect("/courses");
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
