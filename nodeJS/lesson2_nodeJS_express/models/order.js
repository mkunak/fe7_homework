const { Schema, model } = require("mongoose");

const orderSchema = new Schema({
  courses: [
    {
      course: {
        type: Object,
        required: true
      },
      count: {
        type: Number,
        required: true
      }
    }
  ],
  user: {
    name: String, 
    userId: {
      type: Schema.Types.ObjectId,
      ref: "User", // ref нужен, чтобы в последствии мы могли применить метод populate()
      required: true
    }
  },
  date: {
    type: Date,
    default: Date.now // вместо 'required: true' мы применяем поле default. И важно НЕ вызывать функцию Date.now !
  }
});

module.exports = model('Order', orderSchema);