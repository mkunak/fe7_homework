const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const movies = require('./data/data');

const server = express(); // server object is created

// server.use(express.static(path.join(__dirname, 'data')))

server.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'view', 'card.html'));
});

server.listen(3000, () => {
  console.log('server is connected on port 3000');
})