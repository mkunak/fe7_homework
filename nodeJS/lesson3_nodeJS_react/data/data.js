const movies = [
  {
    _id: { $oid: "573a1391f29313caabcd75b5" },
    genres: ["Horror"],
    cast: [
      "Max Schreck",
      "Gustav von Wangenheim",
      "Greta Schrèder",
      "Georg H. Schnell"
    ],
    poster:
      "https://m.media-amazon.com/images/M/MV5BMTAxYjEyMTctZTg3Ni00MGZmLWIxMmMtOGM2NTFiY2U3MmExXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_SX677_AL_.jpg",
    title: "Nosferatu",
    fullplot:
      "Wisbourg, Germany based estate agent Knock dispatches his associate, Hutter, to Count Orlok's castle in Transylvania as the Count wants to purchase an isolated house in Wisbourg. They plan on selling him the one across the way from Hutter's own home. Hutter leaves his innocent wife, Ellen, with some friends while he is away. Hutter's trek is an unusual one, with many locals not wanting to take him near the castle where strange events have been occurring. Once at the castle, Hutter does manage to sell the Count the house, but he also notices and feels unusual occurrences, primarily feeling like there is a dark shadow hanging over him, even in the daytime when the Count is unusually asleep. Hutter eventually sees the Count's sleeping chamber in a crypt, and based on a book he has recently read, believes the Count is really a vampire or Nosferatu. While Hutter is trapped in the castle, the Count, hiding in a shipment of coffins, makes his way to Wisbourg, causing death along his way, which most attribute to the plague. Hutter himself tries to rush home to save his town and most importantly save Ellen from Nosferatu's imminent arrival. In Wisbourg, Ellen can feel the impending darkness as Nosferatu gets closer. But she learns that a sinless woman can sacrifice herself to kill the vampire. Will Hutter be able to save Ellen either from Nosferatu and/or her self-sacrifice?",
    languages: ["German"],
    directors: ["F.W. Murnau"],
    writers: [
      "Henrik Galeen (screen play)",
      'Bram Stoker (based on the novel: "Dracula")'
    ],
    awards: {
      wins: { $numberInt: "1" },
      nominations: { $numberInt: "1" },
      text: "1 win & 1 nomination."
    },
    year: { $numberInt: "1922" },
    countries: ["Germany"],
    type: "movie"
  },
  {
    _id: { $oid: "573a1391f29313caabcd7472" },
    genres: ["Drama"],
    cast: ["Rudolph Christians", "Miss DuPont", "Maude George", "Mae Busch"],
    poster:
      "https://m.media-amazon.com/images/M/MV5BMTk2NDkxMTY1Nl5BMl5BanBnXkFtZTgwNDI1NDU5MTE@._V1_SY1000_SX677_AL_.jpg",
    title: "Foolish Wives",
    fullplot:
      '"Count" Karanzim, a Don Juan is with his cousins in Monte Carlo, living from faked money and the money he gets from rich ladies, who are attracted by his charmes and his title or his militaristic and aristocratic behaviour. He tries to have success with Mrs Hughes, the wife of the new US ambassador.',
    languages: ["English"],
    directors: ["Erich von Stroheim"],
    writers: [
      "Erich von Stroheim (story)",
      "Marian Ainslee (titles)",
      "Walter Anthony (titles)"
    ],
    awards: {
      wins: { $numberInt: "1" },
      nominations: { $numberInt: "0" },
      text: "1 win."
    },
    year: { $numberInt: "1922" },
    countries: ["USA"],
    type: "movie"
  },
  {
    _id: { $oid: "573a1390f29313caabcd587d" },
    genres: ["Biography", "Crime", "Drama"],
    cast: ["John McCann", "James A. Marcus", "Maggie Weston", "Harry McCoy"],
    poster:
      "https://m.media-amazon.com/images/M/MV5BNDkxZGU4NmMtODJlNy00YzA2LTg4ZGMtNGFlNzAyNzcxOTM1XkEyXkFqcGdeQXVyOTM3MjcyMjI@._V1_SY1000_SX677_AL_.jpg",
    title: "Regeneration",
    fullplot:
      "At 10 years old, Owens becomes a ragged orphan when his sainted mother dies. The Conways, who are next door neighbors, take Owen in, but the constant drinking by Jim soon puts Owen on the street. By 17, Owen learns that might is right. By 25, Owen is the leader of his own gang who spend most of their time gambling and drinking. But Marie comes into the gangster area of town and everything changes for Owen as he falls for Marie. But he cannot tell her so, so he comes to her settlement to find education and inspiration. But soon, his old way of life will rise to confront him again.",
    languages: ["English"],
    directors: ["Raoul Walsh"],
    writers: [
      "Owen Frawley Kildare (book)",
      'Raoul Walsh (adapted from the book: "My Mamie Rose")',
      'Carl Harbaugh (adapted from the book: "My Mamie Rose")'
    ],
    awards: {
      wins: { $numberInt: "1" },
      nominations: { $numberInt: "0" },
      text: "1 win."
    },
    year: { $numberInt: "1915" },
    countries: ["USA"],
    type: "movie"
  }
];

module.exports = movies;