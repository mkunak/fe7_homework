const diagnose = require('./diagnose');
const diagnoses = require('./diagnoses');
const calcWeightIndex = require("./calcWeightIndex");

const weight = {
  diagnose,
  diagnoses,
  calc: calcWeightIndex
};

module.exports = weight;
