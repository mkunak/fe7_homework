const diagnoses = require('./diagnoses');

const diagnose = weightIndex => {
  const index = Math.floor((weightIndex - 10) / 5);

  return diagnoses[index] ? diagnoses[index] : diagnoses[diagnoses.length - 1];
  // switch (weightIndex) {
  //   case weightIndex > 10 && weightIndex <= 15:
  //     return "anorexia";
  //   case weightIndex > 15 && weightIndex <= 25:
  //     return "normal weight";
  //   case weightIndex > 25 && weightIndex <= 30:
  //     return "overweight";
  //   case weightIndex > 30 && weightIndex <= 35:
  //     return "I degree of obesity.";
  //   case weightIndex > 35 && weightIndex <= 40:
  //     return "II degree of obesity.";
  //   case weightIndex > 40:
  //     return "III degree of obesity.";
  // }
};

module.exports = diagnose;
