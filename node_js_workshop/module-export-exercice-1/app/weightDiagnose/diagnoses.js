const diagnoses = [
  "anorexia",
  "normal weight",
  "overweight",
  "I degree of obesity.",
  "II degree of obesity.",
  "III degree of obesity."
];

module.exports = diagnoses;
