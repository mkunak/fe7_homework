const express = require("express");
const cors = require("cors");
const bodyParser = require('body-parser');
// const allProductsRoutes = require('./products/getAllProducts')
const routes = require('./routes');

const app = express();
app.use(cors());
app.use(bodyParser.json());
// app.use('/allproducts', allProductsRoutes);
routes(app);

app.get("/", (request, response) => {
  // request - объект запроса где очень много всего
  // response - объект ответа, который мы сами формируем для того, чтобы отправить на фронт
  response.send("Home response has been send on front");
});

app.get("/contacts", (request, response) => {
  response.send("Contact response has been send on front");
});

app.post("/post", (request, response) => {
  response.send(request.body);
  // response.send("Post response has been send to front. Post we use to add data");
});

app.listen(3000, (request, response) => {
  console.log("We are listening app-server throw the PORT=3000", response);
});
