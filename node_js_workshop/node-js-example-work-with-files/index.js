const fs = require("fs");

// fs.readFile("file.txt", "utf-8", (err, data) => {
//  console.log(data);
// });

//fs.readFile("not-found.txt", "utf-8", (err, data) => {
//    if (err) { console.log(err) }
//    console.log(data);
//})
//
// const data = "New File Contents";

// fs.writeFile("new-file.txt", data, (err) => {
//  if (err) console.log(err);
//  console.log("Successfully Written to File.");
// });

fs.unlink('temp.txt', (err) => {
 if (err) throw err;
 console.log('File deleted');
});