const getZodiac = require('./getZodiac');
const getChineseAge = require('./getChineseAge');

const zodiac = {
  getZodiac: getZodiac,
  getChineseAge: getChineseAge,
};

module.exports = zodiac;