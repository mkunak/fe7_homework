const express = require("express");
const cors = require("cors"); // разрешает запросы на домен с любого другого домена
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const passport = require("pasport");

const Course = require('./schema/courses');

const product = mongoose.model("Product", {
  id: Number,
  name: { type: String, required: true, min: 2, max: 120 },
  price: { type: Number, required: true, min: 0 },
  description: { type: String, required: true, min: 2, max: 2000 },
  image: { type: String, required: true },
  category: 
});

// const obj = {
//   id: 1,
//   name: 'Lenovo',
//   price: 1000,
//   description: 'PC',
//   image: 'http://lenovo.com',
//   category: 'notebook'
// }

// const cat = category.find({name: "notebook"});

const category = mongoose.model("Category", {
  id: Number,
  name: { type: String, required: true, min: 2, max: 120 },
});

const app = express();
app.use(cors());
app.use(bodyParser.json());

const url =
  "mongodb+srv://mkunak:y6ft8R84RCbdxGHG@cluster0-nmb6a.mongodb.net/test?retryWrites=true&w=majority";
mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true});

async function getAllCourses() {
  const course = await Course.find().populate();
  console.log(course);
}

const PORT = 3000;

app.listen(PORT, () => {
  console.log(`Port ${PORT} is initialized`);
});