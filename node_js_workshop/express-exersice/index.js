// подключение express
const express = require("express");
// создаем объект приложения
const app = express();
const route = require("./routers");
app.use(json())
// определяем обработчик для маршрута "/"
app.get("/", function(request, response){
    // отправляем ответ
    response.send("<h2>Добро пожаловать, дорогая теща</h2>");
});

app.get("/contacts", function(request, response){
    response.send("<h2>И до свидания</h2>")
});

// начинаем прослушивать подключения на 3000 порту
const PORT = 3000;

app.listen(PORT, () => {
    console.log(`running port on ${PORT}`)
});