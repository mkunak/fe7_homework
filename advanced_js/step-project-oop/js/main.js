let modalPositionCenterX = 0;
let modalPositionCenterY = 0;

const showModalButton = document.querySelector('.button');
const overlay = document.body.querySelector('.overlay');
let visitBord = document.querySelector('.visit-bord');
let visitBordMessage = document.querySelector('.visit-bord__message');

let modal = null;
let formLabel = null;
let formOptions = null;
let closeModalButton = null;

let appointmentParametersArray = [];
let customerDataArray = [];

class Modal {
    constructor() {

    }

    _initModal() {
        overlay.innerHTML = `<div class="modal hidden">
                <h2 class="modal__heading">registration form</h2>
                <div class="form form--flex">
                    <label id="form__label" class="form--flex">
                        <select id="form__options" class="form__options form__text form__options--mb15">
                            <option class="form__text form__text--light" hidden disabled selected>Choose your doctor</option>
                            <option id="cardiologist" class="form__option form__text">Cardiologist</option>
                            <option id="dentist" class="form__option form__text">Dentist</option>
                            <option id="therapist" class="form__option form__text">Therapist</option>
                        </select>
                    </label>
                    <button class="form__btn form__text">Create appointment</button>
                </div>
                <div class="cross">
                    <a class="cross__link" href="#">
                        <i class="fas fa-times"></i>
                    </a>
                </div>
            </div>`;

        let _this = this;
        modal = document.body.querySelector('.modal');
        closeModalButton = document.querySelector('.fa-times');
        formLabel = document.getElementById('form__label');
        formOptions = document.getElementById('form__options');

        showModalButton.addEventListener('click', function () {
            _this.open(modal);
            formOptions.selectedIndex = 0;
            _this.removeElems(formLabel);
            _this.setCenterModal(modal);
        });

        formOptions.selectedIndex = 0;
        this.removeElems(formLabel);
        this.setCenterModal(modal);

        formOptions.addEventListener('change', function () {
            if (formOptions.selectedIndex === 1) {
                formOptions.style.backgroundColor = 'white';
                formOptions.style.borderColor = '#A64D9B';
                _this.removeElems(formLabel);
                _this.insertInputFields(Cardiologist.inputFieldsData, formLabel);
                _this.setCenterModal(modal);
            }
            if (formOptions.selectedIndex === 2) {
                formOptions.style.backgroundColor = 'white';
                formOptions.style.borderColor = '#A64D9B';
                _this.removeElems(formLabel);
                _this.insertInputFields(Dentist.inputFieldsData, formLabel);
                _this.setCenterModal(modal);
            }
            if (formOptions.selectedIndex === 3) {
                formOptions.style.backgroundColor = 'white';
                formOptions.style.borderColor = '#A64D9B';
                _this.removeElems(formLabel);
                _this.insertInputFields(Therapist.inputFieldsData, formLabel);
                _this.setCenterModal(modal);
            }
        });

        const createCardButton = document.querySelector('.form__btn');
        createCardButton.addEventListener('mousedown', function () {
            if (formOptions.selectedIndex === 0) {
                formOptions.style.backgroundColor = 'lightyellow';
                formOptions.style.borderColor = 'red';
            }
            if (formOptions.selectedIndex === 1) { // cardiologist
                let appointment = new Cardiologist(modalWindow.customerValues);
                if (!modalWindow.checkEmptyInputs()) {
                    appointment.createAppointment();
                    modal.classList.add('hidden');
                    overlay.classList.add('hidden');
                }
            }
            if (formOptions.selectedIndex === 2) { // dentist
                let appointment = new Dentist(modalWindow.customerValues);
                if (!modalWindow.checkEmptyInputs()) {
                    appointment.createAppointment();
                    modal.classList.add('hidden');
                    overlay.classList.add('hidden');
                }
            }
            if (formOptions.selectedIndex === 3) { // therapist
                let appointment = new Therapist(modalWindow.customerValues);
                if (!modalWindow.checkEmptyInputs()) {
                    appointment.createAppointment();
                    modal.classList.add('hidden');
                    overlay.classList.add('hidden');
                }
            }
        });

        overlay.addEventListener('mousedown', function (e) {
            if (e.target === overlay) {
                _this.close(modal);
            }
            if (e.target === closeModalButton) {
                _this.close(modal);
            }
        });
    }

    insertInputFields(inputFieldsData, parent) {
        function setAttributes(elem, placeholdersObject, inputFieldName, attributes) {
            elem.setAttribute('data-id', inputFieldName);
            for (let i = 0; i < attributes.length; i++) {
                for (let key in attributes[i]) {
                    elem.setAttribute(key, attributes[i][key]);
                }
            }
            for (let key in placeholdersObject) {
                if (key === inputFieldName) {
                    elem.setAttribute('placeholder', placeholdersObject[key]);
                }
            }
        }

        for (let i = 0; i < inputFieldsData.inputsNames.length; i++) {
            let inputElem = document.createElement('input');
            inputElem.className = inputFieldsData.inputCss;
            setAttributes(inputElem, Modal.placeholders, inputFieldsData.inputsNames[i], inputFieldsData.inputsAttributes);
            parent.insertBefore(inputElem, parent.children[1]);
        }

        for (let i = 0; i < inputFieldsData.textareaNames.length; i++) {
            let textareaElem = document.createElement('textarea');
            textareaElem.className = inputFieldsData.textareaCss;
            setAttributes(textareaElem, Modal.placeholders, inputFieldsData.textareaNames[i], inputFieldsData.textareaAttributes);
            parent.appendChild(textareaElem);
        }
    }

    removeElems(parent) {
        for (let i = 1, len = parent.children.length; i < len; i++) {
            parent.children[1].remove();
        }
    }

    setCenterModal(modalElem) {
        let modalComputed = getComputedStyle(modalElem);
        let modalComputedWidth = Number(modalComputed.width.slice(0, modalComputed.width.length - 2));
        let modalComputedHeight = Number(modalComputed.height.slice(0, modalComputed.height.length - 2));
        modalPositionCenterX = ((document.documentElement.clientWidth - modalComputedWidth) / 2) + 'px';
        modalPositionCenterY = ((document.documentElement.clientHeight - modalComputedHeight) / 2) + 'px';
        modalElem.style.left = modalPositionCenterX;
        modalElem.style.top = modalPositionCenterY;
    }

    open(modalElem) {
        overlay.classList.remove('hidden');
        modalElem.classList.remove('hidden');
    }

    close(modalElem) {
        overlay.classList.add('hidden');
        modalElem.classList.add('hidden');
    }

    get customerValues() {
        const inputs = modal.getElementsByTagName('input');
        const inputsArray = Array.from(inputs);
        const customerValues = {};
        for (let i = 0; i < inputsArray.length; i++) {
            customerValues[inputsArray[i].dataset.id] = inputsArray[i].value;
        }
        customerValues._doctor = formOptions.value;
        if (document.getElementById('additionalInfo')) {
            customerValues._additional = document.getElementById('additionalInfo').value;
        }
        return customerValues;
    }

    checkEmptyInputs() {
        const formInputs = Array.from(document.querySelectorAll('.form__input'));
        const emptyFormInputs = [];
        for (let i = 0; i < formInputs.length; i++) {
            let elem = 0;
            if (!formInputs[i].classList.contains('form__textarea') && formInputs[i].value === '') {
                elem = formInputs.splice(i, 1);
                emptyFormInputs.push(...elem);
                i--;
            }
        }

        for (let i = 0; i < emptyFormInputs.length; i++) {
            emptyFormInputs[i].style.backgroundColor = 'lightyellow';
            emptyFormInputs[i].style.borderColor = 'red';
            emptyFormInputs[i].placeholder = 'Required field';
            emptyFormInputs[i].addEventListener('click', function () {
                emptyFormInputs[i].style.backgroundColor = 'white';
            });
        }
        return emptyFormInputs.length;
    }
}

Modal.placeholders = {
    _name: 'Firstname Lastname',
    _date: 'Preferred date of your visit',
    _reason: 'Reason for visit',
    _age: 'Age',
    _last_visit: 'Last visit date',
    _normal_pressure: 'Normal pressure',
    _bwi: 'Body weight index',
    _past_diseases: 'Past diseases of cardio-vascular system',
    _additional: 'Give us additional information about your health'
};

class Visit {
    constructor(customerData) { // название визита, дата визита, ФИО пациента.
        this._customer_data = customerData;
    }

    createAppointment() {
        if (localStorage.getItem('appointments')) {
            appointmentParametersArray = JSON.parse(localStorage.getItem('appointments'));
            customerDataArray = JSON.parse(localStorage.getItem('customerData'));
            visitBord.innerHTML = '';

            let p = document.createElement('p'); // <p class="visit-bord__message">No items have been added.</p>
            p.className = localStorage.getItem('bordMessage');
            p.textContent = 'No items have been added.';
            visitBord.appendChild(p);

            for (let i = 0; i < appointmentParametersArray.length; i++) {
                Visit.createCard(appointmentParametersArray[i], customerDataArray[i]);
            }

            Visit.card.counter = appointmentParametersArray[appointmentParametersArray.length - 1].counter;
            Visit.card.left = appointmentParametersArray[appointmentParametersArray.length - 1].left;
            Visit.card.top = appointmentParametersArray[appointmentParametersArray.length - 1].top;
        }

        Visit.card.left = Number(Visit.card.left) + 80;
        if (Visit.card.left > 950) {
            Visit.card.left = 30;
        }
        Visit.card.top = Number(Visit.card.top) + 25;
        if (Visit.card.top > 350) {
            Visit.card.top = 10;
        }
        Visit.card.counter++;
        Visit.card.cardClassName = 'card';
        Visit.card.showMoreCss = 'show-more';
        Visit.card.showLessCss = 'show-less hidden';
        Visit.card.paragraphCss = 'card__info hidden';
        Visit.card.textAreaCss = 'card__info card__additional hidden';
        Visit.card.zIndex = appointmentParametersArray.length + 1;

        Visit.createCard(Visit.card, this._customer_data);

        visitBordMessage = document.querySelector('.visit-bord__message'); /**/
        visitBordMessage.classList.add('hidden');
        Visit.bordMessageCss = visitBordMessage.className;

        appointmentParametersArray.push(Visit.card);
        customerDataArray.push(this._customer_data);

        localStorage.setItem('appointments', JSON.stringify(appointmentParametersArray));
        localStorage.setItem('customerData', JSON.stringify(customerDataArray));
        localStorage.setItem('bordMessage', Visit.bordMessageCss);
    }
}

Visit.card = {
    counter: 0,
    left: 0,
    top: 0,
    zIndex: 1,
    cardClassName: 'card',
    textAreaCss: 'card__info card__additional hidden',
    paragraphCss: 'card__info hidden',
    showMoreCss: 'show-more',
    showLessCss: 'show-less hidden'
};

Visit.bordMessageCss = '';

Visit.createCard = function (cardParametersObject, userDataObject) {
    const card = document.createElement('div'); // <div class="card" draggable="true">
    card.className = cardParametersObject.cardClassName;
    card.setAttribute('draggable', 'true');
    card.style.zIndex = cardParametersObject.zIndex; // zIndex

    let crossDiv = document.createElement('div');
    crossDiv.className = 'cross close-card';
    crossDiv.innerHTML = '<a class="cross__link" href="#"><i class="fas fa-times card__cross"></i></a>';
    card.appendChild(crossDiv);

    const cardHeading = document.createElement('h3'); // <h3 class="card__heading">Appointment #</h3>
    cardHeading.className = 'card__heading';
    cardHeading.innerHTML = `Appointment<br>#${cardParametersObject.counter}`;
    card.appendChild(cardHeading);

    for (let key in userDataObject) {
        const paragraph = document.createElement('p');
        const textArea = document.createElement('textarea');
        if (key === '_additional') {
            textArea.className = cardParametersObject.textAreaCss; // cardParametersObject.textAreaCss;
            textArea.textContent = `${userDataObject[key]}`;
            textArea.setAttribute('cols', '30');
            textArea.setAttribute('rows', '10');
            textArea.setAttribute('maxlength', '400');
            textArea.setAttribute('placeholder', 'You gave no additional information about your health');
            textArea.setAttribute('readOnly', 'true');
            card.appendChild(textArea);
            continue;
        }
        if (key !== '_additional') {
            paragraph.textContent = `${userDataObject[key]}`;
            paragraph.className = cardParametersObject.paragraphCss; // cardParametersObject.paragraphCss;
        }
        if (key === '_name' || key === '_doctor') {
            paragraph.className = 'card__info visible';
        }

        card.appendChild(paragraph);
    }

    let showMore = document.createElement('div');
    showMore.className = cardParametersObject.showMoreCss || 'show-more'; // showMoreCss
    showMore.setAttribute('data-show_info', 'true');
    if (showMore.className === 'show-more hidden') {
        showMore.setAttribute('data-show_info', 'false');
    }
    showMore.innerHTML = '<a class="show-more__link" href="#"><i class="fas fa-caret-down"></i></a>';
    card.appendChild(showMore);

    let showLess = document.createElement('div');
    showLess.className = cardParametersObject.showLessCss || 'show-less hidden'; // showLessCss
    showLess.setAttribute('data-show_info', 'false');
    if (!(showLess.className === 'show-less hidden')) {
        showLess.setAttribute('data-show_info', 'true');
    }
    showLess.innerHTML = '<a class="show-less__link" href="#"><i class="fas fa-caret-up"></i></a>';
    card.appendChild(showLess);

    card.style.left = cardParametersObject.left + 'px';
    card.style.top = cardParametersObject.top + 'px';

    visitBord.appendChild(card);

    cardParametersObject.cardClassName = card.className;
};

class Therapist extends Visit { // Если выбрана опция Терапевт: цель визита, возраст, ФИО.
    constructor(customerValues) {
        super(customerValues);
    }

    createAppointment() {
        super.createAppointment();
    }
}

Therapist.inputFieldsData = {
    inputCss: 'form__text form__input',
    inputsNames: ['_name', '_reason', '_age'],
    inputsAttributes: [
        {required: 'true'}
        // {['data-id']: 'lalala'}
    ],
    textareaCss: 'form__text form__input form__textarea form__input--mt20',
    textareaNames: ['_additional'],
    textareaAttributes: [
        {id: 'additionalInfo'},
        {cols: '30'},
        {rows: '4'},
        {maxlength: '400'},
        {['data-id']: '_additional'}
    ]
};

class Dentist extends Visit { // Если выбрана опция Стоматолог: цель визита, дата последнего посещения, ФИО.
    constructor(customerValues) {
        super(customerValues);
    }

    createAppointment() {
        super.createAppointment();
    }
}

Dentist.inputFieldsData = {
    inputCss: 'form__text form__input',
    inputsNames: ['_name', '_reason', '_last_visit'],
    inputsAttributes: [
        {required: 'true'}
    ],
    textareaCss: 'form__text form__input form__textarea form__input--mt20',
    textareaNames: ['_additional'],
    textareaAttributes: [
        {id: 'additionalInfo'},
        {cols: '30'},
        {rows: '4'},
        {maxlength: '400'},
        {['data-id']: '_additional'}
    ]
};

class Cardiologist extends Visit { // Если выбрана опция Кардиолог, появляются следующие поля для ввода информации: цель визита, обычное давление, индекс массы тела, перенесенные заболевания сердечно-сосудистой системы, возраст, ФИО.
    constructor(customerValues) {
        super(customerValues);
    }

    createAppointment() {
        super.createAppointment();
    }
}

Cardiologist.inputFieldsData = {
    inputCss: 'form__text form__input',
    inputsNames: ['_name', '_reason', '_age', '_normal_pressure', '_bwi', '_past_diseases'],
    inputsAttributes: [
        {required: 'true'}
    ],
    textareaCss: 'form__text form__input form__textarea form__input--mt20',
    textareaNames: ['_additional'],
    textareaAttributes: [
        {id: 'additionalInfo'},
        {cols: '30'},
        {rows: '4'},
        {maxlength: '400'},
        {['data-id']: '_additional'}
    ]
};

let modalWindow = new Modal();
modalWindow._initModal();

visitBord.addEventListener('mousedown', function (e) {
    if (e.target.closest(`.card`)) {
        const currentCard = e.target.closest(`.card`);
        let currentCardNumber = Number(currentCard.querySelector('.card__heading').textContent.slice(12));

        const cardCloseButton = e.target.closest(`.close-card`);
        if (cardCloseButton) {
            currentCard.classList.add('hidden');
            appointmentParametersArray[currentCardNumber - 1].cardClassName = currentCard.className;
            localStorage.setItem('appointments', JSON.stringify(appointmentParametersArray));
        }

        let cardsHidden = Array.from(document.querySelectorAll('.card.hidden'));
        let cardsVisible = Array.from(document.querySelectorAll('.card'));
        visitBordMessage = document.querySelector('.visit-bord__message');
        if (cardsHidden.length === cardsVisible.length) {
            visitBordMessage.classList.remove('hidden');

            Visit.bordMessageCss = visitBordMessage.className;
            localStorage.setItem('bordMessage', Visit.bordMessageCss);
        }
        if (cardsHidden.length !== cardsVisible.length) {
            visitBordMessage.classList.add('hidden');

            Visit.bordMessageCss = visitBordMessage.className;
            localStorage.setItem('bordMessage', Visit.bordMessageCss);
        }

        for (let i = 0; i < cardsVisible.length; i++) {
            cardsVisible[i].style.zIndex = 0;
            appointmentParametersArray[i].zIndex = 0;
        }
        currentCard.style.zIndex = 1;
        appointmentParametersArray[currentCardNumber - 1].zIndex = currentCard.style.zIndex;
        localStorage.setItem('appointments', JSON.stringify(appointmentParametersArray));

        let cardOffsetX = 0;
        let cardOffsetY = 0;
        currentCard.addEventListener('dragstart', function (e) {
            this.style.opacity = '0.5';
            cardOffsetX = e.offsetX;
            cardOffsetY = e.offsetY;
        });
        currentCard.addEventListener('dragend', function (e) {
            this.style.opacity = '1';
            this.style.left = (e.pageX - visitBord.getBoundingClientRect().left - cardOffsetX) + 'px';
            this.style.top = (e.pageY - visitBord.getBoundingClientRect().top - cardOffsetY) + 'px';

            let condition1 = Number(this.style.left.slice(0, this.style.left.length - 2)) < 0;
            let condition2 = Number(this.style.left.slice(0, this.style.left.length - 2)) > (visitBord.getBoundingClientRect().width - this.getBoundingClientRect().width);
            let condition3 = Number(this.style.top.slice(0, this.style.top.length - 2)) < 0;
            let condition4 = Number(this.style.top.slice(0, this.style.top.length - 2)) > (visitBord.getBoundingClientRect().height - this.getBoundingClientRect().height);

            if (condition1) {
                this.style.left = 0 + 'px';
            }
            if (condition2) {
                this.style.left = (visitBord.getBoundingClientRect().width - this.getBoundingClientRect().width) + 'px';
            }
            if (condition3) {
                this.style.top = 0 + 'px';
            }
            if (condition4) {
                this.style.top = (visitBord.getBoundingClientRect().height - this.getBoundingClientRect().height) + 'px';
            }

            appointmentParametersArray[currentCardNumber - 1].left = Number(this.style.left.slice(0, this.style.left.length - 2));
            appointmentParametersArray[currentCardNumber - 1].top = Number(this.style.top.slice(0, this.style.top.length - 2));
            localStorage.setItem('appointments', JSON.stringify(appointmentParametersArray));
        });

        let showMoreButton = currentCard.querySelector('.show-more');
        let showLessButton = currentCard.querySelector('.show-less');
        let cardInfoArray = Array.from(currentCard.querySelectorAll('.card__info'));
        let argsArray = [];
        for (let i = 0; i < cardInfoArray.length; i++) {
            if (cardInfoArray[i].classList.contains('visible')) {
                let argElem = cardInfoArray.splice(i, 1);
                argsArray.push(argElem[0]); // cardInfoArray.splice(i, 1);
                i--;
            }
        }
        cardInfoArray.unshift(...argsArray);

        if (e.target.closest(`.show-more`) && showMoreButton.className === 'show-more') {
            showMoreButton.classList.add('hidden');
            showLessButton.classList.remove('hidden');
            for (let i = 0; i < cardInfoArray.length; i++) {
                cardInfoArray[i].classList.remove('hidden');
            }
            appointmentParametersArray[currentCardNumber - 1].paragraphCss = 'card__info';
            appointmentParametersArray[currentCardNumber - 1].textAreaCss = 'card__info card__additional';
            appointmentParametersArray[currentCardNumber - 1].showMoreCss = showMoreButton.className;
            appointmentParametersArray[currentCardNumber - 1].showLessCss = showLessButton.className;
            localStorage.setItem('appointments', JSON.stringify(appointmentParametersArray));
        }

        if (e.target.closest(`.show-less`) && showLessButton.className === 'show-less') {
            showMoreButton.classList.remove('hidden');
            showLessButton.classList.add('hidden');
            for (let k = 0; k < cardInfoArray.length; k++) {
                cardInfoArray[k].classList.add('hidden');
                if (cardInfoArray[k].classList.contains('visible')) {
                    cardInfoArray[k].classList.remove('hidden');
                }
            }
            appointmentParametersArray[currentCardNumber - 1].paragraphCss = 'card__info hidden';
            appointmentParametersArray[currentCardNumber - 1].textAreaCss = 'card__info card__additional hidden';
            appointmentParametersArray[currentCardNumber - 1].showMoreCss = showMoreButton.className;
            appointmentParametersArray[currentCardNumber - 1].showLessCss = showLessButton.className;
            localStorage.setItem('appointments', JSON.stringify(appointmentParametersArray));
        }
    }
});

if (localStorage.getItem('appointments')) {
    appointmentParametersArray = JSON.parse(localStorage.getItem('appointments'));
    customerDataArray = JSON.parse(localStorage.getItem('customerData'));
    visitBord.innerHTML = '';

    let p = document.createElement('p'); // <p class="visit-bord__message">No items have been added.</p>
    p.className = localStorage.getItem('bordMessage');
    p.textContent = 'No items have been added.';
    visitBord.appendChild(p);

    for (let i = 0; i < appointmentParametersArray.length; i++) {
        Visit.createCard(appointmentParametersArray[i], customerDataArray[i]); // cardParametersObject, userDataObject
    }
}