'use strict';
// let's try to pass function arguments with a help of destructuring assignment

console.log('example 1:'); // example 1:
const argumentObject1 = {
    title: 'Autoshop "Magelan"',
    phone: '0442014541',
    adress: {
        city: 'Kyiv',
        street: 'Onore de Balsak',
        house: 25
    },
    account: undefined
};

showArgumentValues1(argumentObject1);

function showArgumentValues1({title, phone, adress, account = 'AN1245RO'}) {
    console.log(title);
    console.log(phone);
    console.log(adress);
    console.log(account);
}

console.log('example 2:'); // example 2:
const argumentObject2 = {
    title: 'Autoshop "Magelan"',
    phone: '0442014541',
    adress: {
        city: 'Kyiv',
        street: 'Onore de Balsak',
        house: 25
    },
    account: undefined
};

showArgumentValues2(argumentObject2);

function showArgumentValues2({
                                 title,
                                 phone: p,
                                 adress: a,
                                 account: acc = 'AN1245RO'}) {
    console.log(title);
    console.log(p);
    console.log(a);
    console.log(acc);
}