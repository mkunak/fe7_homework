// 'use strict';

console.log('example 1:'); // example 1:
const arrOfNames = ['John', 'Lea', 'Joahim', 'Francua', 'Helena'];
let [name1, , name2, ...name3] = arrOfNames;
console.log(name1, name2, name3);

console.log('example 2:');// example 2:
let string1 = 'abc';
let [var1, var2, var3] = string1;
console.log(var1, var2, var3);

console.log('example 3:');// example 3:
let a = 2;
let b = 3;
[a, b] = [b, a];// перестановка деструктуризацией массива
console.log(a); // 3
console.log(b); // 2




console.log('example 3:'); // example 3:
let string2 = 'Arnold Rease';
let person = {};
[person.name, person['family name']] = string2.split(' ');
console.log(person);

console.log('example 4:'); // example 4:
const product = {
    title: 'Notebook',
    // price: 1200,
    width: 380,
    height: 20,
    color: 'green',
    'Wi-Fi': false
};
let {title, color: productColor, price: productPrice = 1100} = product;
console.log(product);
console.log(title, productColor, productPrice);
productColor = 'violet-grey';
title = 'Laptop';
console.log(product.color, productColor);
console.log(product.title, title);

console.log('example 5:'); // example 5:
const tabouret = new Map();
tabouret.set('legs quantity', 4);
tabouret.set('height, cm', 50);
tabouret.set('width, cm', 40);
tabouret.set('depth, cm', 40);
tabouret.set('arms availability', false);
tabouret.set('price, EUR', 25);
console.log(tabouret);
for (let [key, value] of tabouret) {
    console.log(key, ':', value);
}