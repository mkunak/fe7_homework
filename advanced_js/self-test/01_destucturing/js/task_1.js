// const user = {
//     name: 'John',
//     age: 30
// };
//
// let {name, age, isAdmin = false} = user;
//
// console.log(name);
// console.log(age);
// console.log(isAdmin);

// let users = [
//     {name: "Joe", age: 42},
//     {name: "Den", age: 31},
//     {name: "Tom", age: 19},
// ];
//
// for(let {name, age} of users) {
//     console.log(name, age);
// }
// let obj = {name: "Den", age: 31};
// let {name: visibleName, age = 18} = obj;
// let registeredUser = {visibleName, age};
// console.log(visibleName, age);

// var iteration = 5;
// var sum = 5;
// {
//     let iteration = 10;
//     var add = function(val) {
//         if(!val) val = 1;
//         iteration += val;
//         return iteration;
//     }
// }
// console.log( add(sum) );

// let a = 1;
// let b = 2;
// {
//     let a = 3;
//     b = b + a;
// }
// let sum = a + b;
// console.log( sum );

function Product(additionalPrice) {
    let basePrice = 10;
    this.showPrice = function() {
        return basePrice + additionalPrice;
    }
}
let laptop = new Product(20);
console.log( laptop.showPrice() );