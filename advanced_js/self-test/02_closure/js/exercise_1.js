// example 1:
// let name = 'Ivanko';
// function sayHello() {
//     console.log('Hello, ' + name + '!');
// }
// name = 'Catherine';
// sayHello();


// example 2:
// (function g() {
//     return 1;
// })();
// console.log(g);

// example 3:
// console.log( 'example 3:' );
// function sayHi(name) {
//     var phrase = "Привет, " + name;
//     console.log( phrase );
// }
// sayHi('Вася');

// example 4:
console.log( 'example 4:' );
function makeCounter() {
    let currentCount = 1;
    return function() { // (**)
        return currentCount++;
    };
}
let counter = makeCounter(); // (*)
// каждый вызов увеличивает счётчик и возвращает результат:
console.log( counter() ); // 1
console.log( counter() ); // 2
console.log( counter() ); // 3
console.log( makeCounter()() ); // 3
console.log( makeCounter()() ); // 3

let counter2 = makeCounter();
// создаем другой счётчик, он будет независим от первого:
console.log( counter2() ); // 1

// example 5:
console.log( 'example 5:' );
let a = 1;
let b = 2;
console.log( a, b );
{
    console.log(  b );
    let a = 3;
    console.log( a, b );
    b += a;
    console.log( a, b );
}
console.log( a, b );
let sum = a + b;
console.log( sum );

// example 6:
console.log( 'example 6:' );

function createAdd() {
    let start = 1;
    return function(add) {
        return start += add;
    }
}
let add1 = createAdd();
let add2 = createAdd();
let result = add1(1) + add1(2) + add2(1);
console.log( result );


// example 7:
console.log( 'example 7:' );
const x = {}
x.method = function() {
    this.name = "Jane Doe"
    const z = () => console.log(this.name);
    // const z = function() {
    //     console.log(this.name);
    // };
    console.log(z());
};
x.name = "John Doe";
x.method();
// console.log( 'example 6:' );