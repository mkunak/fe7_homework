const names = [
    {name: 'Jack'},
    {name: 'John'},
    {name: 'Jill'},
    {name: 'Jane'},
    {name: 'Jerry'},
    {name: 'Jessy'}
];

const btn = document.getElementById('btn');

const showName = displayName(names, '#text');

btn.addEventListener('click', showName);

function displayName(array, selector) {
    let counter = 0;
    const text = document.querySelector(selector);
    const arrayOfNames = array;
    return function () {
        ++counter;
        if (counter >= arrayOfNames.length) {
            counter = 0;
        }
        text.textContent = names[counter].name;
    };
}