function Hamster() {
    this.stomach = [];
    this.eat = function(food) {
        this.stomach.push(food);
    }
}
// let speedy = {
    // stomach: [],
    // __proto__:Hamster
// };
let speedy = new Hamster();

// let lazy = {
    // stomach: [],
    // __proto__:Hamster
// };
let lazy = new Hamster();

speedy.name = 'Speedy';
lazy.name = 'Lazy';

speedy.eat('apple');

console.log(speedy, speedy.stomach);
console.log(lazy, lazy.stomach);