// const stool = {
//     legs: 4,
//     size: {
//         width: 400,
//         depth: 400,
//         height: 520
//     },
//     material: 'wood'
// };

// function A() {
//     return stool;
// }
//
// function B() {
//     return Object.assign(stool);
// }
//
// let a = new A();
// let b = Object.assign(stool);

// let c = undefined;
// console.log(c == false);

// a.category = 1;
// b.category = 0;
//
// console.log(a === b);
// console.log(a, b);

// const stoolProto = Object.getPrototypeOf(stool);
// const chair = Object.setPrototypeOf(null);
// const chair = Object.create(stoolProto);
// const chair = {};

// console.log(chair);
// console.log(stool);

// class Animal {
//     constructor(name) {
//         this.name = name;
//     }
// }
//
// class Rabbit extends Animal{
//     jump() {
//         console.log(this.name + " jumps.");
//     }
// }
//
// let theBrave = new Rabbit("The Brave");
// theBrave.jump();

// class Animal {
//     static sayHi() {console.log("Hello World!")};
// }
//
// class Rabbit extends Animal {
//
// }
//
// Rabbit.sayHi();

// try {
//     notAssignedVariable;
// } catch (e) {
//     console.error(e);
//     console.error(e.name);
//     console.error(e.message);
//     console.error(e.stack);
// }

function foo() {
    try {
        return 1;
    } catch(e) {
        console.log(2);
    } finally {
        console.log(3);
    }
}
console.log(foo());