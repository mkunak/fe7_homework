console.log('example 1:');
let person = {
  name: 'Jack',
  weight: 100,
  sayHi: function (whom) {
      console.log(`${this.name} say Hi to ${whom}!`);
  }
};
person.sayHi('Mary');
// setTimeout(person.sayHi.bind(person, 'Mary'), 1000);

console.log('example 2:');
let prototypeObj = {
    property: 1,
    setProperty: function(value) {
        this.property += value;
    }
};
let obj = {};
obj.__proto__ = prototypeObj;
obj.setProperty(2);
console.log(prototypeObj);
console.log(obj);

console.log('example 3:');
function Animal() {
    this.moveType = this.__proto__.moveType;
}
let Fish = {
    moveType: "swim"
};
let Rabbit = {
    moveType: "jump"
};
Animal.__proto__ = Fish;
Animal.prototype = Rabbit;
let theBrave = new Animal();

console.log(Fish);
console.log(Rabbit);
console.log(theBrave.moveType);
console.log(Animal.moveType);