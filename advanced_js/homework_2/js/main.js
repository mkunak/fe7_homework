'use strict';

/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
class Hamburger {
    constructor(size, stuffing) {
        try {
            if (!arguments.length) {
                throw new HamburgerException('\nThere are no SIZE and STUFFING arguments.');
            }

            if (arguments.length > 0 && arguments[0].type !== 'size') {
                throw new HamburgerException('\nIncorrect data entering. \nPlease, enter only two parameters: SIZE parameter first, STUFFING parameter second!');
            }

            if (arguments.length === 2 && arguments[1].type !== 'stuffing') {
                throw new HamburgerException('\nIncorrect data entering. \nPlease, enter only two parameters: SIZE parameter first, STUFFING parameter second!');
            }

            if (arguments.length > 2) {
                throw new HamburgerException('\nIncorrect data entering. \nPlease, enter only two parameters: SIZE parameter first, STUFFING parameter second!');
            }
        } catch (e) {
            return console.error(e.name + ": " + e.message);
        }

        this._size = size;
        this._stuffing = stuffing;
        this._toppings = [];
    }

    set addTopping(topping) {
        try {
            if (!arguments.length) {
                throw new HamburgerException('There is no TOPPING argument.');
            }

            if (arguments.length === 1 && arguments[0].type !== 'topping') {
                throw new HamburgerException('Incorrect data entering. \n Please, enter only a TOPPING parameter!');
            }

            if (arguments.length > 1) {
                throw new HamburgerException('Incorrect data entering. \n Please, enter only a TOPPING parameter!');
            }

            if (this._toppings.some(isToppingIn)) {
                throw new HamburgerException(`The TOPPING_${topping.name.toUpperCase()} has been already added.`);
            } else {
                this._toppings.push(topping);
                return;
            }

        } catch (e) {
            console.error(e.name + ": " + e.message);
        }

        function isToppingIn(item) {
            return (item.name === topping.name);
        }
    };

    set removeTopping(topping) {
        try {
            if (!arguments.length) {
                throw new HamburgerException('There is no TOPPING argument.');
            }

            if (arguments.length === 1 && arguments[0].type !== 'topping') {
                throw new HamburgerException('Incorrect data entering. \n Please, enter only a TOPPING parameter!');
            }

            if (arguments.length > 1) {
                throw new HamburgerException('Incorrect data entering. \n Please, enter only a TOPPING parameter!');
            }

            if (!this._toppings.length || !this._toppings.some(function (item) {
                return (item.name === topping.name);
            })) {
                throw new HamburgerException(`\nYou are trying to remove not existing kind of TOPPING. \nTOPPING_${topping.name.toUpperCase()} is not found.`);
            }

            this._toppings = this._toppings.filter(function (item) {
                return (item.name !== topping.name);
            });

        } catch (e) {
            console.error(e.name + ": " + e.message);
        }
    };

    get toppings() {
        const toppingsArray = [`no kind of TOPPING`];

        for (let i = 0; i < this._toppings.length; i++) {
            toppingsArray[i] = `TOPPING_${this._toppings[i].name.toUpperCase()}`;
        }
        return `Hamburger toppings: ${toppingsArray}`;
    };

    get size() {
        try {
            if (this._size === undefined) {
                throw new HamburgerException('\nSIZE parameter is not found. \nPlease, enter SIZE parameter!');
            } else {
                return `Hamburger size: SIZE_${this._size.name.toUpperCase()}`;
            }
        } catch (e) {
            console.error(e.name + ": " + e.message);
        }
    };

    get stuffing() {
        try {
            if (this._stuffing === undefined) {
                throw new HamburgerException('\nSTUFFING parameter is not found. \nPlease, enter STUFFING parameter!');
            } else {
                return `Hamburger stuffing: STUFFING_${this._stuffing.name.toUpperCase()}`;
            }
        } catch (e) {
            console.error(e.name + ": " + e.message);
        }
    };

    calculatePrice() {
        try {
            if (this._size === undefined || this._stuffing === undefined) {
                throw new HamburgerException('\nIncorrect data entering. \nPlease, enter only two parameters: SIZE parameter first, STUFFING parameter second!');
            }
        } catch (e) {
            console.error(e.name + ": " + e.message);
        }

        let priceTotal = 0;
        priceTotal = this._size.price + this._stuffing.price;
        for (let i = 0; i < this._toppings.length; i++) {
            if (this._toppings[i]) {
                priceTotal += this._toppings[i].price;
            }
        }
        return `Total price: ${priceTotal} UAH`;
    };

    calculateCalories() {
        try {
            if (this._size === undefined || this._stuffing === undefined) {
                throw new HamburgerException('\nIncorrect data entering. \nPlease, enter only two parameters: SIZE parameter first, STUFFING parameter second!');
            }
        } catch (e) {
            console.error(e.name + ": " + e.message);
        }

        let caloriesTotal = 0;
        caloriesTotal = this._size.price + this._stuffing.price;
        for (let i = 0; i < this._toppings.length; i++) {
            if (this._toppings[i]) {
                caloriesTotal += this._toppings[i].price;
            }
        }
        return `Total calories: ${caloriesTotal} cal`;
    };
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
    type: 'size',
    name: 'small',
    price: 50,
    calories: 20
};

Hamburger.SIZE_LARGE = {
    type: 'size',
    name: 'large',
    price: 100,
    calories: 40
};

Hamburger.STUFFING_CHEESE = {
    type: 'stuffing',
    name: 'cheese',
    price: 10,
    calories: 20
};

Hamburger.STUFFING_SALAD = {
    type: 'stuffing',
    name: 'salad',
    price: 20,
    calories: 5
};

Hamburger.STUFFING_POTATO = {
    type: 'stuffing',
    name: 'potato',
    price: 20,
    calories: 5
};

Hamburger.TOPPING_SPICE = {
    type: 'topping',
    name: 'spice',
    price: 15,
    calories: 0
};

Hamburger.TOPPING_MAYO = {
    type: 'topping',
    name: 'mayo',
    price: 20,
    calories: 5
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
class HamburgerException {
    constructor(message) {
        this.message = message;
        this.name = "HamburgerException";
    }
}

// const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO);
const hamburger = new Hamburger();
// const hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_SALAD);

// hamburger.addTopping = Hamburger.SIZE_SMALL;
hamburger.addTopping = Hamburger.TOPPING_MAYO;
hamburger.addTopping = Hamburger.TOPPING_SPICE;
// hamburger.addTopping;
// hamburger.addTopping = Hamburger.TOPPING_MAYO;
// hamburger.addTopping = Hamburger.TOPPING_SPICE;

hamburger.removeTopping = Hamburger.TOPPING_MAYO;
hamburger.removeTopping = Hamburger.TOPPING_SPICE;
// hamburger.removeTopping = Hamburger.TOPPING_MAYO;
// hamburger.removeTopping = Hamburger.TOPPING_SPICE;

console.log(hamburger.toppings);
console.log(hamburger.size);
console.log(hamburger.stuffing);

console.log(hamburger.calculatePrice());
console.log(hamburger.calculateCalories());

console.log(hamburger);