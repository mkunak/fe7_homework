'use strict';

/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
    try {
        if (!arguments.length) {
            throw new HamburgerException('There are no SIZE and STUFFING arguments.');
        }

        if (arguments.length > 0 && arguments[0].type !== 'size') {
            throw new HamburgerException('Incorrect data entering. \n Please, enter only two parameters: SIZE parameter first, STUFFING parameter second!');
        }

        if (arguments.length === 2 && arguments[1].type !== 'stuffing') {
            throw new HamburgerException('Incorrect data entering. \n Please, enter only two parameters: SIZE parameter first, STUFFING parameter second!');
        }

        if (arguments.length > 2) {
            throw new HamburgerException('Incorrect data entering. \n Please, enter only two parameters: SIZE parameter first, STUFFING parameter second!');
        }


    } catch (e) {
        console.error(e.name + ": " + e.message);
    }
    this._size = size.name;
    this._stuffing = stuffing.name;
    this._toppings = [];

    prices.size = size.price;
    prices.stuffing = stuffing.price;
    calories.size = size.calories;
    calories.stuffing = stuffing.calories;
}

const prices = {
    size: 0,
    stuffing: 0,
    toppings: {
        mayo: 0,
        spice: 0
    }
};

const calories = {
    size: 0,
    stuffing: 0,
    toppings: {
        mayo: 0,
        spice: 0
    }
};

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
    type: 'size',
    name: 'small',
    price: 50,
    calories: 20
};
// Hamburger.SIZE_SMALL = 'small';

Hamburger.SIZE_LARGE = {
    type: 'size',
    name: 'large',
    price: 100,
    calories: 40
};
// Hamburger.SIZE_LARGE = 'large';

Hamburger.STUFFING_CHEESE = {
    type: 'stuffing',
    name: 'cheese',
    price: 10,
    calories: 20
};
// Hamburger.STUFFING_CHEESE = 'cheese';

Hamburger.STUFFING_SALAD = {
    type: 'stuffing',
    name: 'salad',
    price: 20,
    calories: 5
};
// Hamburger.STUFFING_SALAD = 'salad';

Hamburger.STUFFING_POTATO = {
    type: 'stuffing',
    name: 'potato',
    price: 20,
    calories: 5
};
// Hamburger.STUFFING_POTATO = 'potato';

Hamburger.TOPPING_SPICE = {
    type: 'topping',
    name: 'spice',
    price: 15,
    calories: 0
};
// Hamburger.TOPPING_SPICE = 'spice';

Hamburger.TOPPING_MAYO = {
    type: 'topping',
    name: 'mayo',
    price: 20,
    calories: 5
};
// Hamburger.TOPPING_MAYO = 'mayo';

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function addTopping(topping) {
    try {
        if (!arguments.length) {
            throw new HamburgerException('There is no TOPPING argument.');
        }

        if (arguments.length > 0 && arguments[0].type !== 'topping') {
            throw new HamburgerException('Incorrect data entering. \n Please, enter only a TOPPING parameter!');
        }

        if (arguments.length > 1) {
            throw new HamburgerException('Incorrect data entering. \n Please, enter only a TOPPING parameter!');
        }

        for (let i = 0; i <= this._toppings.length; i++) {
            if (this._toppings[i] === topping.name) {
                throw new HamburgerException(`The TOPPING_${topping.name.toUpperCase()} has been already added.`);
            }
            if (this._toppings[i] === undefined) {
                this._toppings.push(topping.name);
                prices.toppings[topping.name] = topping.price;
                calories.toppings[topping.name] = topping.calories;
                return;
            }
        }
    } catch (e) {
        console.error(e.name + ": " + e.message);
    }
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function removeTopping(topping) {
    try {
        if (this._toppings.length === 0) {
            throw new HamburgerException(`You are trying to remove not existing kind of TOPPING. TOPPING_${topping.name.toUpperCase()} is not found.`);
        }
        this._toppings.forEach(function (item, i, array) {
            if (item === topping.name) {
                array.splice(i, 1);
                prices.toppings[topping.name] = 0;
                calories.toppings[topping.name] = 0;
                return;
            }
            if (item !== topping.name && i === array.length - 1) {
                throw new HamburgerException(`You are trying to remove not existing kind of TOPPING. TOPPING_${topping.name.toUpperCase()} is not found.`);
            }
        });
    } catch
        (e) {
        console.error(e.name + ": " + e.message);
    }
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function getToppings() {
    const toppingsArray = [];
    if (!this._toppings.length) {
        return `Hamburger toppings: no kind of TOPPING`;
    }
    for (let i = 0; i < this._toppings.length; i++) {
        toppingsArray[i] = `TOPPING_${this._toppings[i].toUpperCase()}`;
    }
    return `Hamburger toppings: ${toppingsArray}`;
};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function getSize() {
    return `Hamburger size: SIZE_${this._size.toUpperCase()}`;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function getStuffing() {
    return `Hamburger stuffing: STUFFING_${this._stuffing.toUpperCase()}`;
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    function addValuesToArrayFromObject(sourceObject) {
        let targetArray = [];
        let sourceArray = Object.entries(sourceObject);
        for (let i = 0; i < sourceArray.length; i++) {
            targetArray.push(...sourceArray[i]);
        }
        return targetArray;
    }

    let priceTotal = 0;
    let pricesArray = [];

    pricesArray.push(...addValuesToArrayFromObject(prices));
    for (let i = 0; i < pricesArray.length; i++) {
        if (typeof (pricesArray[i]) === 'number') {
            priceTotal += pricesArray[i];
        }
        if (typeof (pricesArray[i]) === 'object') {
            pricesArray.push(...addValuesToArrayFromObject(pricesArray[i]));
            i--;
            pricesArray.splice(i, 2);
        }
    }
    return `Total price: ${priceTotal} UAH`;
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    function addValuesToArrayFromObject(sourceObject) {
        let targetArray = [];
        let sourceArray = Object.entries(sourceObject);
        for (let i = 0; i < sourceArray.length; i++) {
            targetArray.push(...sourceArray[i]);
        }
        return targetArray;
    }

    let caloriesTotal = 0;
    let caloriesArray = [];

    caloriesArray.push(...addValuesToArrayFromObject(calories));
    for (let i = 0; i < caloriesArray.length; i++) {
        if (typeof (caloriesArray[i]) === 'number') {
            caloriesTotal += caloriesArray[i];
        }
        if (typeof (caloriesArray[i]) === 'object') {
            caloriesArray.push(...addValuesToArrayFromObject(caloriesArray[i]));
            i--;
            caloriesArray.splice(i, 2);
        }
    }
    return `Total calories: ${caloriesTotal} cal`;
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException(message) {
    this.message = message;
    this.name = "HamburgerException";
}

// const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
const hamburger = new Hamburger();
// const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO);
// const hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_SALAD);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// hamburger.addTopping(Hamburger.SIZE_SMALL);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// hamburger.addTopping(Hamburger.TOPPING_SPICE);
// hamburger.removeTopping(Hamburger.TOPPING_MAYO);
// hamburger.removeTopping(Hamburger.TOPPING_MAYO);
// hamburger.removeTopping(Hamburger.TOPPING_SPICE);
// hamburger.removeTopping(Hamburger.TOPPING_MAYO);
// hamburger.removeTopping(Hamburger.TOPPING_SPICE);

console.log(hamburger.getToppings());
console.log(hamburger.getSize());
console.log(hamburger.getStuffing());

console.log(hamburger.calculatePrice());
console.log(hamburger.calculateCalories());

console.log(hamburger);