// Welcome. In this kata, you are asked to square every digit of a number.
// For example, if we run 9119 through the function, 811181 will come out, because 9^2 is 81 and 1^2 is 1.
// Note: The function accepts an integer and returns an integer.
//////////////////////////////////////////////////////////////////
function squareDigitsOfNumber(number) {
  if (number === Math.round(number)) {
    let str = '';
    let array = [];
    for (let i = 0; i < `${number}`.length; i++) {
      array[i] = +`${number}`.slice(i, i + 1);
    }
    array.forEach((item) => str += `${Math.pow(item, 2)}`);
    return +str;
  }
}

console.log(squareDigitsOfNumber(9119));