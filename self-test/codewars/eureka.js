function sumDigPow(a, b) { // 89 = 8^1 + 9^2
  if (a > 0 && b > a) {
    let array = [];
    for (let i = a; i <= b; i++) {
      let result = 0;
      if (i < 10) array.push(i);
      if (i >= 10) {
        let newArr = [];
        for (let j = 0; j < `${i}`.length; j++) {
          newArr[j] = +`${i}`.slice(j, j + 1);
        }
        newArr.forEach((item, index) => result += Math.pow(item, index + 1));
        if (result === i) array.push(i);
      }
    }
    return array;
  } else return 'enter positive numbers only!';
}

console.log(sumDigPow(1, 10000000));