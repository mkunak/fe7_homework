$(document).ready(function () {

    // $(window).on('resize', function () {

        if ($(window).width() <= 640) {
            $('.drop-down').hide();

            $('.toggler').on('mouseup', function (e) {
                if ($('.drop-down').is(':hidden')) {
                    $('.drop-down').fadeIn(300);
                    $('.toggler').addClass('active');
                    console.log(`hi`);
                } else if ($('.drop-down').is(':visible')) {
                    $('.drop-down').fadeOut(400);
                    $('.toggler').removeClass('active');
                }
                console.log(e.target);
            });

            for (let i = 0; i < $('.drop-down__item').length; i++) {
                $('.drop-down__item').eq(i).on('mouseup', function () {
                    $('.drop-down').fadeOut(400);
                });
            }

            $(document).on('mousedown', function (e) { // событие клика по веб-документу
                const div = $('.drop-down'); // обращение к элементу - выпадающему списку
                if (!div.is(e.target) // если клик был не по моему элементу
                    && div.has(e.target).length === 0 // и не по его дочерним элементам
                    && $('.drop-down').is(':visible')) { // и элемент видим, то ...
                    div.fadeOut(400); // скрываем его
                }
                $('.toggler').removeClass('active');
            });
        }
        if ($(window).width() > 640) {
            $('.drop-down').show();
        }

    // });

});

// console.log(webgazer);


webgazer.setGazeListener(function(data, elapsedTime) {
    if (data == null) {
        return;
    }
    let xprediction = data.x; //these x coordinates are relative to the viewport
    let yprediction = data.y; //these y coordinates are relative to the viewport
    console.log(elapsedTime); //elapsed time is based on time since begin was called
    console.log(xprediction, '|', yprediction); //elapsed time is based on time since begin was called
}).begin();