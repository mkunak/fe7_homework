import React from 'react';
import { connect } from 'react-redux';

import { logout, doLogin } from '../../store/login/actions';
import {getPlanetAction} from '../../store/planets/actions'

class Login extends React.Component {
  state = {
    inputValue: '',
  };

  handleLoginButton = () => {
    const { doLogin, getPlanetAction } = this.props;
    const { inputValue } = this.state;

    doLogin(inputValue);
    getPlanetAction(1);
  }
  handleChange = (event) => {
    this.setState({
      inputValue: event.target.value
    })
  }

  render() {
    const { logout, user, error } = this.props;

    return (
      <div>
        {error &&(
          <div>
            {error.message}
          </div>
        )}

        {user && (
          <>
            <p>{user.name}</p>

            <button onClick={logout}>
              Logout
            </button>
          </>
        )}

        {!user && (
          <>
            <input placeholder="password ..." onChange={this.handleChange} />
            <button onClick={this.handleLoginButton}>
              Login
            </button>
          </>
        )}
      </div>
    );
  }
};

const mapStateToProps = function(state) {
  return {
    user: state.user,
    error: state.error,
  };
};

const mapDispatchToProps = {
  logout,
  doLogin,
  getPlanetAction,
};

const enhancer = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default enhancer(Login);
