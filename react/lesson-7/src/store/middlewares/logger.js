const logger = function(store) {
  return function(next) {
    return function(action) {
      console.warn(new Date().toString(), action);

      fetch('http://localhost:3002', {
        method: 'post',
        headers: {
          'content-type': 'application/json',
        },
        body: JSON.stringify({
          action,
        }),
      })

      next(action);
    };
  };
};

export default logger;
