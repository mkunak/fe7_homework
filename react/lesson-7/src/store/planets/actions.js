import api from '../../services/api';
import generateAsyncActionCreator from '../utils/generateAsyncActionCreator';

export const SAVE_PLANET = 'SAVE_PLANET'
export const ERROR_ACTION = 'ERROR_ACTION'
export const SAVE_RESIDENT = 'SAVE_RESIDENT'

export function savePlanet(payload) {
  return {
    type: SAVE_PLANET,
    payload,
  };
}

export function errorAction(payload) {
  return {
    type: ERROR_ACTION,
    payload,
  };
}
export function saveResident(payload) {
  return {
    type: SAVE_RESIDENT,
    payload,
  };
}

export const getPlanetAction = generateAsyncActionCreator({
  apiService: api.getPlanet,
  successAction: savePlanet,
  errorAction: errorAction,
});

export const getResidentAction = generateAsyncActionCreator({
  apiService: api.getResident,
  saveResident,
  errorAction,
});
