import { ERROR_ACTION, SAVE_PLANET } from './actions';

export default function(state, action) {
  switch (action.type) {
    case ERROR_ACTION:
      return {
        isError: true,
        error: action.payload,
      };

    case SAVE_PLANET:
      return {
        isError: false,
        planets: action.payload
      };


    default:
      return state;
  }
};