import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import loginReducer from './login/reducer';
import retryLogin from './middlewares/retryLogin';
import getChildrenEntities from './middlewares/getChildrenEntities';

const initialValue = {
  user: null,
};

const enhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  loginReducer,
  initialValue,
  enhancer(applyMiddleware(thunk, getChildrenEntities, retryLogin)),
);

export default store;
