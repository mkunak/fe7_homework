import { LOGIN, LOGOUT, PROMOTION } from './actionNames'

export function login(payload) {
  return {
    type: LOGIN,
    payload,
  };
}

export function logout() {
  return {
    type: LOGOUT,
  };
}

export function promotion() {
  return {
    type: PROMOTION,
  };
}