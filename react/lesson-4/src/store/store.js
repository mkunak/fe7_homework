import { createStore } from 'redux'
import reducer from './reducer';

const initialState = {
  user: {
    id: 1,
    name: 'John Dow',
    age: 16,
    userImage: `https://randomuser.me/api/portraits/men/1.jpg`,
  }, // { ... }
};

const store = createStore(reducer, initialState);

console.log(store);
console.log(store.subscribe(() => {console.log('store subscribe was called ', store.getState())}));

export default store;
