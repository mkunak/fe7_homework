import React from 'react';
import {connect} from 'react-redux';
import {logout, login} from '../../actions/actions';
import store from '../../store/store';
import UserCard from "../UserCard";

const handleClick = function (props, store) {
  console.log(store);
  // return (props.login({
  //   name: props.user.name, //'Vasya',
  //   age: props.user.age, //16,
  //   userImage: props.user.userImage, //'https://randomuser.me/api/portraits/men/65.jpg',
  // }));
};

const Login = function (props) {
  console.warn(props);

  return (
    <div>
      <p>{props.user ? <UserCard user={props.user}/> : 'no user'}</p>
      <button onClick={props.logout}>
        Logout
      </button>
      <button onClick={handleClick(props)}>
        Login
      </button>
    </div>
  );
};

const mapStateToProps = function (state) {
  return {
    user: state.user,
    userId: 10,
  };
};

const mapDispatchToProps = {
  logout,
  login,
};

const enhancer = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default enhancer(Login);
