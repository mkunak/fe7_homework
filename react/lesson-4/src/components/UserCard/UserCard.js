import React from 'react';
import styles from './UserCard.module.css';
import store from '../../store/store';
import { connect } from 'react-redux';
import {logout, login} from '../../actions/actions';

function UserCard(props) {
  console.log(props);

  return (
    <div className={styles.user_card}>
      <p>{props.user.name}</p>
      <p>{props.user.age} years old</p>
      <img src={props.user.userImage} alt='user_foto'/>
    </div>
  );
}

export default UserCard;