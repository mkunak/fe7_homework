import React from 'react';
import {Route, Link} from "react-router-dom";
import {connect} from 'react-redux';

import styles from './Header.module.scss';

import City from '../../pages/City';
import Home from '../../pages/Home';
import About from '../../pages/About';

import store from "../../store/store";

const handleClick = () => {
  // store.dispatch({type: 'ADD_DATA', data: null});
  // store.dispatch({type: 'ADD_ID', id: null});
};

const handleBtnClick = () => {
  // store.dispatch({type: 'OPEN_MODAL', isModalOpen: true});
};

function Header() {

  return (
    <div>
      <header className={styles.header}>
        <div className="container p-0">
          <ul className={styles.list}>
            <li className={styles.navItem}>
              <Link className={styles.navLink} onClick={handleClick} to="/">Home</Link>
            </li>
            <li className={styles.navItem}>
              <Link className={styles.navLink} onClick={handleClick} to="/city">City</Link>
            </li>
            <li className={styles.navItem}>
              <Link className={styles.navLink} onClick={handleClick} to="/about">About</Link>
            </li>
          </ul>
        </div>
      </header>
      <Route exact path='/' component={Home}/>
      <Route path='/city' component={City}/>
      <Route path='/about' component={About}/>
    </div>
  );
}

// }

export default connect(
  state => ({state}),
  dispatch => ({})
)(Header);