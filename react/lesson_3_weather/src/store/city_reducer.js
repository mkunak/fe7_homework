
const initialState = {
  id: null,
  cityName: null,
  data: null,
  isModalOpen: false,
};

export const city_reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'OPEN_MODAL':
      return state = {
        ...state,
        isModalOpen: action.isModalOpen
      };
    case 'CITY_NAME':
      return state = {
        ...state,
        cityName: action.cityName
      };
    case 'ADD_DATA':
      return state = {
        ...state,
        data: action.data
      };
    case 'ADD_ID':
      return state = {
        ...state,
        id: action.id
      };
    default:
      return state;
  }
};
