import { createStore } from 'redux';
import {city_reducer} from './city_reducer';

const store = createStore(city_reducer);

store.subscribe(() => {
  console.log(store.getState());
});

export default store;
