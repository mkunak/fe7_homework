export const cities = [
  {
    "id": 703448,
    "name": "Kiev",
    "country": "UA",
    "coord": {
      "lon": 30.516666,
      "lat": 50.433334
    }
  },
  {
    "id": 1855566,
    "name": "Nashizukuri",
    "country": "JP",
    "coord": {
      "lon": 135.666672,
      "lat": 34.783329
    }
  },
  {
    "id": 2147035,
    "name": "Terrey Hills",
    "country": "AU",
    "coord": {
      "lon": 151.233337,
      "lat": -33.683331
    }
  },
  {
    "id": 2864463,
    "name": "Neundorf",
    "country": "DE",
    "coord": {
      "lon": 11.21667,
      "lat": 50.25
    }
  },
  {
    "id": 3112031,
    "name": "Renche",
    "country": "ES",
    "coord": {
      "lon": -7.29054,
      "lat": 42.739891
    }
  },
  {
    "id": 2660892,
    "name": "Ennetburgen",
    "country": "CH",
    "coord": {
      "lon": 8.33333,
      "lat": 46.98333
    }
  },
  {
    "id": 3447799,
    "name": "Estado de Sergipe",
    "country": "BR",
    "coord": {
      "lon": -37.5,
      "lat": -10.5
    }
  },
  {
    "id": 294098,
    "name": "Nazareth",
    "country": "IL",
    "coord": {
      "lon": 35.295559,
      "lat": 32.703609
    }
  },
  {
    "id": 1925051,
    "name": "Meiling",
    "country": "CN",
    "coord": {
      "lon": 115.745018,
      "lat": 28.79224
    }
  },
  {
    "id": 773176,
    "name": "Dobczyce",
    "country": "PL",
    "coord": {
      "lon": 20.089359,
      "lat": 49.881088
    }
  },
  {
    "id": 4333161,
    "name": "Merrydale",
    "country": "US",
    "coord": {
      "lon": -91.108437,
      "lat": 30.501301
    }
  },
  {
    "id": 2968765,
    "name": "Villefranche-sur-Cher",
    "country": "FR",
    "coord": {
      "lon": 1.76667,
      "lat": 47.299999
    }
  }
];