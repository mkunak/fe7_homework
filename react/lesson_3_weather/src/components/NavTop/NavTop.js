import React from 'react';
import {Nav} from 'react-bootstrap';
import {connect} from 'react-redux';

import NavButton from '../NavButton/NavButton';
import styles from './NavTop.module.scss';

function NavTop() {
  return (
    <nav>
      <div className="container d-flex justify-content-between">
        <Nav defaultActiveKey="/home" as="ul" className={styles.nav_top_links}>
          <Nav.Item as="li">
            <Nav.Link href="/home">Active</Nav.Link>
          </Nav.Item>
          <Nav.Item as="li">
            <Nav.Link eventKey="link-1">Link</Nav.Link>
          </Nav.Item>
          <Nav.Item as="li">
            <Nav.Link eventKey="link-2">Link</Nav.Link>
          </Nav.Item>
        </Nav>
        <NavButton/>
      </div>
    </nav>
  );
}

export default connect(
  state => ({state})
)(NavTop);