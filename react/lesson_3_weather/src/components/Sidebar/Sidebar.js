
import React from 'react';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";

import {cities} from "../../store/cities";
import styles from './Sidebar.module.scss';
import store from '../../store/store';


function handleClick(e) {
  const cityName = e.target.innerHTML;
  store.dispatch({type: 'CITY_NAME', cityName});

  cities.forEach(item => {
    if (item.name === cityName) {
      store.dispatch({type: "ADD_ID", id:item.id});
    }
  });
}

function Sidebar() {
  let cityItems = cities.map((city) => {
    return (
      <li className="list-group-item list-group-item-action bg-light"
          className={styles.cities__item}
          key={city.id}
          onClick={handleClick}>
        <Link className={styles.cities__link} to={`/city/${city.name}`}>{city.name}</Link>
      </li>
    )
  });

  return (
    <div className="d-inline-block" id="sidebar-wrapper">
      <ul className="list-group list-group-flush pb-5" className={styles.cities}>
        <li className="sidebar-heading"
            className={styles.cities__heading}
            key={0}>Choose City
        </li>
        {cityItems}
      </ul>
    </div>
  );
}

function mapStateToProps() {
  
}

export default connect(
  state => ({state})
)(Sidebar);
