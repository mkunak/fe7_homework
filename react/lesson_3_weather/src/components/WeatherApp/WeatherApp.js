import React from 'react';
import {connect} from 'react-redux';

import Loader from "../Loader";
import store from '../../store/store';
import Weather from "./Weather";

class WeatherApp extends React.Component {
  state = {
    id: null,
    data: null
  };

  componentDidMount() {
    this.setState({
      id: this.props.state.id
    });
    this.requestData(this.state.id);
  }

  componentDidUpdate(prevProps) {
    if (this.props.state.id !== prevProps.state.id) {
      this.requestData(this.props.state.id);
    }
  }

  requestData = (id) => {
    const APPID = 'c47409b885a224800ae22e368c2b1a23';
///////////
    if (this.props.state.data) {
      store.dispatch({type: 'ADD_DATA', data: null});
    }
////////////////
    if (id) {
      fetch(`http://api.openweathermap.org/data/2.5/forecast?id=${id}&APPID=${APPID}`)
        .then(response => response.json())
        .then(data => {
          console.log(data);
          store.dispatch({type: 'ADD_DATA', data});
          this.setState({
            id: data.id,
            data: data,
          })
        })
        .catch(err => {
          console.log(err);
        });
    }
  };

  render() {
    const data = this.props.state.data;
    const id = this.props.state.id;

    console.log(data, '|', id);

    if (!data && id) {
      return (
        <div>
          <Loader/>
        </div>
      );
    }

    if (!data) {
      return <div/>
    }

    return <Weather />;
  }
}

const mapStateToProps = () => {

};

export default connect(
  state=>({state})
)(WeatherApp);