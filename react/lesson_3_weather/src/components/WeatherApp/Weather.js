import React from "react";

import styles from "./Weather.module.scss";
import {connect} from "react-redux";

function Weather(props) {
  const data = props.state.data;

  return (
    <div id="weather" className={styles.weather}>
      <img className={styles.weather__flag}
           src={`https://www.countryflags.io/${(data.city.country).toLowerCase()}/shiny/64.png`}
           alt="flag"/>
      <p className={styles.weather__city}>{data.city.name}</p>
      <p>Temperature: {Math.round(data.list[0].main.temp - 273)}</p>
      <img className={styles.weather__icon}
           src={`http://openweathermap.org/img/wn/${data.list[0].weather[0].icon}@2x.png`}
           alt="weather"/>
      <p>Last update: {data.list[0].dt_txt}</p>
    </div>
  );
}

export default connect(
  state=>({state})
)(Weather);
