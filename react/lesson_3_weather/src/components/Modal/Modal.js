import React from 'react';
import {connect} from "react-redux";
import {Modal} from 'react-bootstrap';
import ModalHeader from 'react-bootstrap/ModalHeader';
import ModalTitle from 'react-bootstrap/ModalTitle';
import ModalBody from 'react-bootstrap/ModalBody';
import ModalFooter from 'react-bootstrap/ModalFooter';
import Button from 'react-bootstrap/Button';

import styles from './Modal.module.scss';
import UserForm from '../LoginForm/LoginForm';

function ModalCentered(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className="justify-content-center"
    >
      <ModalHeader >
        <ModalTitle id="contained-modal-title-vcenter">
          Type your login in
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <UserForm />
      </ModalBody>
      <ModalFooter>
        <Button onClick={props.onHide}>Close</Button>
      </ModalFooter>
    </Modal>
  );
}

export default connect(
  state => ({state})
)(ModalCentered);
