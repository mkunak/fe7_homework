import React from 'react';
import styles from './Loader.module.css';

class Loader extends React.Component {
  render() { // console.log(styles);
    return (
      <div className={styles.lds_position}>
        <div id='loader' className={styles.lds_ripple}>
          <div/>
          <div/>
        </div>
      </div>
    );
  }
}

export default Loader;
