import React from 'react';
import {connect} from 'react-redux';
import {Navbar, InputGroup, Form, FormControl, Button} from 'react-bootstrap';


function LoginForm() {
  return (
    <Navbar className="bg-light justify-content-between">
      <Form inline>
        <InputGroup>
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            placeholder="Username"
            aria-label="Username"
            aria-describedby="basic-addon1"
          />
        </InputGroup>
      </Form>
      <Button type="submit">Submit</Button>
    </Navbar>
  );
}

export default connect(
  state => ({state})
)(LoginForm);