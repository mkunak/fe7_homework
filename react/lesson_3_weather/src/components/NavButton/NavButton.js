import React from 'react';
import Button from "react-bootstrap/Button";
import ButtonToolbar from "react-bootstrap/ButtonToolbar";

import ModalCentered from "../Modal/Modal";

function NavButton() {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <ButtonToolbar >
      <Button variant="outline-light" onClick={() => setModalShow(true)}>
        Login
      </Button>
      <ModalCentered
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </ButtonToolbar>
  );
}

export default NavButton;
