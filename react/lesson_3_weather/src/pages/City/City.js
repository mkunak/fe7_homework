import React from 'react';
import {connect} from "react-redux";
// import {BrowserRouter} from 'react-router-dom';
// import {Route, Link} from "react-router-dom";

import Weather from "../../components/WeatherApp";
import Sidebar from "../../components/Sidebar";

class City extends React.Component {
  render() {
    return (
      <div className='container'>
        <div>
          <h1 className='text-center py-5'>City page!</h1>
        </div>
        <Sidebar/>
        <Weather/>
      </div>
    );
  }
}

export default connect(
  state => ({state}),
  dispatch => ({})
)(City);