import React from "react";
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";

import Modal from './components/Modal';
import NavTop from './components/NavTop/NavTop';
import Header from './sections/Header/Header';
import store from "./store/store";

function App() {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <NavTop/>
        <Header/>
      </Provider>
    </BrowserRouter>
  );
}


export default App;