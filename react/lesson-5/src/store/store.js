import { createStore } from 'redux'

import reducer from './reducer';

const initialValue = {
  user: {
    name: 'Vasya',
    age: 16,
    userImage: 'https://lorempixel.com/100/100',
  }, // { ... }
};

const store = createStore(reducer, initialValue)

export default store;
