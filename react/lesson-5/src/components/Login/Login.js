import React from 'react';
import { connect } from 'react-redux';
import {logout, login} from '../../store/actions';

class Login extends React.Component {
  // console.warn(props);

  componentDidMount() {
    api.loginUser();
  }

  render() {
    const {logout, login, user} = this.props;

    return (
      <div>
        <p>{props.user ? props.user.name : 'no user' }</p>
        <button onClick={props.logout}>
          Logout
        </button>
        <button onClick={()=>props.login({
          name: 'Vasya',
          age: 16,
          userImage: 'https://lorempixel.com/100/100',
        })}>
          Login
        </button>
      </div>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    user: state.user,
    userId: 10,
  };
};

const mapDispatchToProps = {
  logout,
  login,
};

const enhancer = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default enhancer(Login);
