import React from 'react';
import useData from '../../shared/hooks/useData';
import useForm from '../../shared/hooks/useForm';

export default function LoginHook() {
  const URL = 'https://swapi.co/api/people/1';
  const {data, isLoading, error} = useData(URL);
  
  console.log({data, isLoading});
  
  const {
    value: loginValue,
    setInputValue: setLoginValue
  } = useForm();
  
  const {
    value: passwordValue,
    setInputValue: setPasswordValue
  } = useForm();
  
  return (
    <React.Fragment>
      <input type="text" placeholder="login" value={loginValue} onChange={setLoginValue}/>
      <input type="text" placeholder="password" value={passwordValue} onChange={setPasswordValue}/>
      
      <button type="submit">submit</button>
      <div>{isLoading && "Loading..."}</div>
      <div>{data && JSON.stringify(data)}</div>
      <div>{error && "Smth went wrong as we expected!"}</div>
    </React.Fragment>
  );
}
