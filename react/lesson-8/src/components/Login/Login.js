import React from 'react';
import { connect } from 'react-redux';

import Forms from '../../shared/Forms';

import { logout, doLogin } from '../../store/login/actions';
import { getPlanetAction } from '../../store/planets/actions';

const MIN_PASSWORD_LENGTH = 8;

class Login extends React.Component {
  handleLoginButton = () => {
    const { doLogin, getPlanetAction } = this.props;

    // doLogin(inputValue);
    getPlanetAction(1);
  };

  checkErrors = (fieldName, value) => {
    if (fieldName === 'password' && value.length < MIN_PASSWORD_LENGTH) {
      return 'error';
    }
  };

  render() {
    const { logout, user, error } = this.props;

    return (
      <div>
        {error && <div>{error.message}</div>}

        {user && (
          <>
            <p>{user.name}</p>

            <button onClick={logout}>Logout</button>
          </>
        )}

        {!user && (
          <>
            <Forms
              inputs={[
                {
                  name: 'email',
                  placeholder: 'email ...',
                },
                {
                  name: 'password',
                  placeholder: 'password ...',
                },
              ]}
              checkErrors={this.checkErrors}
            />

            <button onClick={this.handleLoginButton}>Login</button>
          </>
        )}
      </div>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    user: state.user,
    error: state.error,
  };
};

const mapDispatchToProps = {
  logout,
  doLogin,
  getPlanetAction,
};

const enhancer = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default enhancer(Login);
