const baseUrl = 'https://swapi.co/api';

export default {
  loginUser() {
    const userId = 1;

    return fetch(`${baseUrl}/people/${userId}`)
      .then(response => response.json());
  },

  getPlanet(type, id) {
    return fetch(`${baseUrl}/${type}/${id}`)
      .then(response => response.json());
  },

  getResident(id) {
    return fetch(id)
      .then(response => response.json());
  },
  
  getData(url) {
    return fetch(url)
      .then(response => response.json());
  },
};