import React from 'react';
import { Provider } from 'react-redux';

import store from './store';

import Login from './components/Login';
import LoginHook from './components/LoginHook';

function App() {
  return (
    <Provider store={store}>
      {/*<Login />*/}
      <LoginHook/>
    </Provider>
  );
}

export default App;
