import { doLogin } from '../login/actions';

const retryLogin = function(store) {
  return function(next) {
    return function(action) {
      if (action.type === 'ERROR') {
        store.dispatch(doLogin());

        return;
      }

      next({
        ...action,
        date: new Date(),
      });
    };
  };
};

export default retryLogin;
