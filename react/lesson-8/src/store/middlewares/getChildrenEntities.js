import { SAVE_PLANET, getResidentAction } from '../planets/actions'

const getChildrenEntities = function(store) {
  return function(next) {
    return function(action) {
      if (action.type === SAVE_PLANET) {
        action.payload.residents.forEach((resident) => {
          store.dispatch(getResidentAction(resident));
        });
      }

      next(action);
    };
  };
};

export default getChildrenEntities;
