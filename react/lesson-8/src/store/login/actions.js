import api from '../../services/api';

import { SUCCESS_LOGIN, FAILURE_LOGIN, LOGOUT, PROMOTION, ERROR } from './actionNames';

function successLogin(payload) {
  return {
    type: SUCCESS_LOGIN,
    payload,
  };
}

function failureLogin(payload) {
  return {
    type: FAILURE_LOGIN,
    payload,
  };
}

function errorLogin(payload) {
  return {
    type: ERROR,
    payload,
  };
}

export function doLogin(password) {
  return function(dispatch) {
    api.loginUser().then((data) => {
      if (data.birth_year === password) {
        dispatch(successLogin(data));
      } else {
        dispatch(failureLogin({
          message: 'Invalid password!',
        }));
      }
    }).catch((error) => {
      dispatch(errorLogin(error))
    });
  }
}

export function logout() {
  return {
    type: LOGOUT,
  };
}

export function promotion() {
  return {
    type: PROMOTION,
  };
}