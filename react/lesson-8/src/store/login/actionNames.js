export const SUCCESS_LOGIN = 'SUCCESS_LOGIN';
export const FAILURE_LOGIN = 'FAILURE_LOGIN';
export const LOGOUT = 'LOGOUT';
export const PROMOTION = 'PROMOTION';
export const ERROR = 'ERROR';
