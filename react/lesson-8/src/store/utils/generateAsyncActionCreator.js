import { errorAction } from "../planets/actions";

const generateAsyncActionCreator = function({
  apiService,
  successAction,
  srejectAction,
}) {
  return function(...values) {
    return function(dispatch) {
      apiService(...values)
        .then((data) => {
          dispatch(successAction(data));

          return data;
        })
        .catch((err) => {
          dispatch(errorAction)
        });
    }
  }
};

export default generateAsyncActionCreator;