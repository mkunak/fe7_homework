import { createStore, compose, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { reducer as reduxFormReducer } from 'redux-form';

import loginReducer from './login/reducer';
import planetsReducer from './planets/reducer';
import retryLogin from './middlewares/retryLogin';
import getChildrenEntities from './middlewares/getChildrenEntities';

const enhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
  login: loginReducer,
  planets: planetsReducer,
  form: reduxFormReducer,
});

const store = createStore(
  rootReducer,
  undefined,
  enhancer(applyMiddleware(thunk, getChildrenEntities, retryLogin))
);

export default store;
