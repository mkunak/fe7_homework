import { ERROR_ACTION, SAVE_PLANET } from './actions';

const initialState = {
  planets: [],
  error: null,
  isError: false,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ERROR_ACTION:
      return {
        isError: true,
        error: action.payload,
      };

    case SAVE_PLANET:
      return {
        isError: false,
        planets: action.payload
      };


    default:
      return state;
  }
};