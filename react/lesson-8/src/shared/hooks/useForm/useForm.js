import {useState, useCallback} from 'react';

const useForm = function () {
  const [value, setValue] = useState('');
  
  const setInputValue = useCallback((event) => {
      setValue(event.target.value);
  }, []);
  
  return {
    value,
    setInputValue,
  }
};

export default useForm;