import {useState, useEffect} from 'react';
import api from '../../services/api';


export default function (url) {
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  
  useEffect(() => {
    setIsLoading(true);
    api.getData(url)
      .then(data => {
        console.log(data);
        setData(data);
        // setIsLoading(false);
      })
      .catch(err => {
        console.log(err);
        // setIsLoading(false);
        setError(err);
      })
      .finally(()=>setIsLoading(false));
  }, []);
  
  return {data, isLoading};
  
}