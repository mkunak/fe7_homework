import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

class Forms extends React.Component {
  handleSubmit = (event) => {
    // fetch or something else... this.props.values

    event.preventDefault();
  }

  render() {
    const { inputs, dirty } = this.props;

    return (
      <div>
        {!dirty && (
          <div>
            Please, fill out the form!
          </div>
        )}

        <form onSubmit={this.handleSubmit}>
          {inputs.map(input => {
            return <Field component="input" name={input.name} placeholder={input.placeholder} key={input.name} />;
          })}

          <input type="submit" value="Submit" />
        </form>
      </div>
    );
  }
}

const mapStateToProps = function({ form }) {
  return {
    values: form.forms ? form.forms.values : {},
  };
};

export default reduxForm({
  form: 'forms',
})(
  connect(mapStateToProps)(Forms)
);
