export default class BookstoreService {
  data = [
    {
      id: 1,
      title: "Toil & Trouble",
      author: "Augusten Burroughs",
      price: 23.45,
      coverImg:
        "https://images-na.ssl-images-amazon.com/images/I/51Q-a%2Bq9AZL._SX322_BO1,204,203,200_.jpg"
    },
    {
      id: 2,
      title: "The Guardians",
      author: "John Grisham",
      price: 14.15,
      coverImg:
        "https://images-na.ssl-images-amazon.com/images/I/61Kqq0Mwb6L._SX342_.jpg"
    },
    {
      id: 3,
      title: "When We Believed in Mermaids",
      author: "Barbara O'Neal",
      price: 18.63,
      coverImg:
        "https://images-na.ssl-images-amazon.com/images/I/41DPq03IAOL.jpg"
    },
    {
      id: 4,
      title: "Blowout: Corrupted Democracy",
      author: "Rachel Maddow",
      price: 4.85,
      coverImg:
        "https://images-na.ssl-images-amazon.com/images/I/41yhagwT3PL._SX326_BO1,204,203,200_.jpg"
    }
  ];

  getBooks() {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(this.data);
      }, 5000);
    });
  }
}
