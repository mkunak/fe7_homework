import React, { Fragment } from "react";
import { connect } from "react-redux";
// import { bindActionCreators } from "redux";

import { withBookstoreService } from "../hoc";
import { booksLoaded } from "../../actions";
import BookListItem from "../BookListItem";
import Spinner from "../Spinner";

import "./BookList.css";

class BookList extends React.Component {
  componentDidMount() {
    const { bookstoreService, booksLoaded } = this.props;
    bookstoreService.getBooks().then(data => {
      console.log(data);
      return booksLoaded(data);
    });
  }

  render() {
    const { books, loading } = this.props;

    if (loading) {
      return (
        <Fragment>
          <Spinner />
        </Fragment>
      );
    }

    return (
      <ul className="book-list">
        {books.map(book => {
          return (
            <li key={book.id}>
              <BookListItem book={book} />
            </li>
          );
        })}
      </ul>
    );
  }
}

const mapStateToProps = state => {
  return {
    books: state.books,
    loading: state.loading
  };
};

// const mapDispatchToProps = (dispatch) => {
// return bindActionCreators({ booksLoaded: booksLoaded }, dispatch);
// };
const mapDispatchToProps = { booksLoaded };

export default withBookstoreService()(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(BookList)
);
