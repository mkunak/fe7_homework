import React from 'react';
import './Spinner.css';

export default class Spinner extends React.Component {
  
  render() {
    return (
      <div>Loading is in progress...</div>
    );
  }
}
