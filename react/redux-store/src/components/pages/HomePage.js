import React from "react";
import BookList from "../BookList";
// import BookstoreService from "../../services/BookstoreService";
import ShoppingCartTable from "../ShoppingCartTable";

// const bookstoreService = new BookstoreService();

const HomePage = () => {
  return (
    <div>
      <h2 className="text-center">Home Page</h2>
      <BookList />
      <ShoppingCartTable />
    </div>
  );
};

export default HomePage;
