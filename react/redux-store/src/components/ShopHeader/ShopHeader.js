import React from "react";
import { Link } from "react-router-dom";

import "./ShopHeader.css";

const ShopHeader = ({ numItems, total }) => {
  return (
    <header className="shop-header row">
      <Link className="logo text-dark" to="/">
        <span>Redux Store</span>
      </Link>
      <Link to="/cart">
        <span className="shopping-cart">
          <i className="cart-icon fa fa-shopping-cart" />
          {numItems} items (${total})
        </span>
      </Link>
    </header>
  );
};

export default ShopHeader;
