import React, { createRef } from 'react';

import Label from "../../Shared/Label";
import ChildButton from "./ChildButton";

import { getNextCounter } from './utils';

import styles from "./MyButton.module.scss";

const addColor = function(elem) {
    elem.style.color = 'red';
};

class MyButton extends React.Component {
    state = {
        counter: 0,
    };

    buttonRef = createRef();
    labelRef = createRef();

    handleClick = () => {
        this.setState({
            counter: getNextCounter(this.state.counter),
        });
    };

    componentDidMount() {
        addColor(this.buttonRef.current);
    }

    render() {
        const { counter } = this.state;
        const { buttonTitle, valueName } = this.props;

        return (
            <div>
                <button
                    className={styles.button}
                    style={{
                        transform: `rotate(${counter}deg)`,
                    }}
                    onClick={this.handleClick}
                    ref={this.buttonRef}
                >
                    {buttonTitle}
                </button>

                {counter === 126 && (
                    <ChildButton
                        handleChildButton={this.handleClick}
                        counter={counter}
                        refLink={this.labelRef}
                    />
                )}

                <Label color="green">Hello, World!</Label>
            </div>
        );
    }
}

export default MyButton;