import React from 'react';

const ChildButton = function({
  handleChildButton,
  counter,
  valueName,
  refLink,
}) {
  return (
    <>
      <p ref={refLink}>
          Current value: {counter} {valueName}
      </p>

      <button onClick={handleChildButton}>
        click me again!
      </button>
    </>
  );
};

export default ChildButton;
