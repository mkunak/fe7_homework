import React from 'react';
import { thisTypeAnnotation } from '@babel/types';

class LoadingWrapper extends React.Component {
  state = {isLoading : true};

  componentDidMount(){
    setTimeout(() => {
      this.setState({isLoading : false});
    }, 1000);
  }

  render() {
    const { children } = this.props;
    const { isLoading } = this.state;

    if(isLoading){
      return <p>Loading</p>
    }

    return (
      <div>
        {children}
      </div>
    );
  }
}

export default LoadingWrapper;