import React from 'react';

const List = function ({items}) {
  return (
    <div>
      <ul>
        {items.map((val) => {
          return (
            <li key={val.src}>
              <img src={val.src}/>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default List;
