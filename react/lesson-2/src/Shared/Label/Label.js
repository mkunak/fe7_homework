import React from 'react';

import styles from './Label.module.css';

const Label = function({ children, color }) {
    console.warn(children, color);

    return (
      <h3 style={ { color } }>
          {children}
      </h3>
    );
};

Label.defaultProps = {
  children: 'placeholder...',
  color: 'red',
};

export default Label;