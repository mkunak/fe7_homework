import React from 'react';

import MyButton from "./components/MyButton";
import Label from "./Shared/Label";
import List from './Shared/List';
import Wrapper from './components/Wrapper';

const App = function () {
  return (
    <Wrapper>
      <MyButton buttonTitle="click me" valueName="click"/>
      
      <Label value="Label in App.js" className="my-class-name"/>
      
      <List
        items={[
          {
            src: 'https://via.placeholder.com/150',
          },
          {
            src: 'https://via.placeholder.com/200',
          },
          {
            src: 'https://via.placeholder.com/250',
          },
        ]}
      />
    </Wrapper>
  );
};

export default App;
