import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";

import actions from '../../actions';


const Counter = ({counter, inc, dec, rnd}) => {

  return (
    <div className="jumbotron d-flex flex-column">
      <h2 className="text-center text-info mb-4">I will know Redux</h2>
      <h4 className="text-center">Counter</h4>
      <div className="align-self-center border">
        <button onClick={inc} id='inc' className="btn btn-success btn-large text-center mx-2">INC</button>
        <button onClick={dec} className="btn btn-danger btn-large text-center mx-2" id='dec'>DEC</button>
        <button onClick={rnd} className="btn btn-info btn-large text-center mx-2" id='rnd'>ADD RND</button>
      </div>
      <p className="text-center h3 " id="counter">{counter}</p>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    counter: state
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(actions, dispatch);
};

// export default connect(mapStateToProps, mapDispatchToProps)(Counter);
export default connect(mapStateToProps, actions)(Counter);
