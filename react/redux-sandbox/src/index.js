import React from 'react';
import ReactDOM from 'react-dom';
import {createStore} from "redux";
import {Provider} from 'react-redux';

import App from "./components/App";

import reducer from "./reducers";

const store = createStore(reducer);
//const {dispatch} = store;

//const {inc: incDispatch, dec: decDispatch, rnd: rndDispatch} = bindActionCreators(actions, dispatch);

// const bindActionCreator = (actionCreator, dispatch) => (...arg) => {
//   dispatch(actionCreator(...arg));
// };
// const decDispatch = bindActionCreators(dec, dispatch);
// const rndDispatch = bindActionCreators(rnd, dispatch);

store.subscribe(() => {
  console.log("Store is changed: ", store.getState());
});

// document.getElementById("inc").addEventListener("click", incDispatch);
//
// document.getElementById("dec").addEventListener("click", decDispatch);
//
// document.getElementById("rnd").addEventListener("click", () => {
//   const payload = Math.floor(Math.random() * 10);
//   rndDispatch(payload);
// });

// const update = () => {
  ReactDOM.render(
    <Provider store={store}>
      <App/>
    </Provider>, document.getElementById('root'));
// };

// update();
//
// store.subscribe(update);
