import '../index.css';
import '../bootstrap.min.css';


export default class SwapiService {
  constructor() {
    this._apiBase = 'https://swapi.co/api/';
  }

  async getResource(url) {
    const response = await fetch(`${this._apiBase}${url}`);
    if (!response.ok) {
      throw new Error(`Could not fetch ${this._apiBase}${url}. Response status ${response.status}.`);
    }
    return await response.json();
  };

  getAllPeople = async () => {
    const response = await this.getResource(`people/`);
    return response.results.map((person) => this._transformPerson(person));
  };

  getPerson = async (id) => {
    const person = await this.getResource(`people/${id}`);
    return this._transformPerson(person);
  };

  getAllPlanets = async () => {
    const allPlanets = await this.getResource('planets/');
    return allPlanets.results.map((planet) => this._transformPlanet(planet));
  };

  getPlanet = async (id) => {
    const planet = await this.getResource(`planets/${id}`);
    return this._transformPlanet(planet);
  };

  getAllStarships = async () => {
    const allStarships = await this.getResource('starships/');
    return allStarships.results.map((starship) => this._transformStarship(starship));
  };

  getStarship = async (id) => {
    const starship = await this.getResource(`starships/${id}`);
    return this._transformStarship(starship);
  };

  getAllVehicles = async () => {
    const allVehicles = await this.getResource('vehicles/');
    // console.log(allVehicles.results.map((vehicle) => this._transformVehicle(vehicle)));
    return allVehicles.results.map((vehicle) => this._transformVehicle(vehicle));
  };

  getVehicle = async (id) => {
    const vehicle = await this.getResource(`vehicles/${id}`);
    return this._transformVehicle(vehicle);
  };

  _extractID(item) {
    const idRegExp = /\/([0-9]*)\/$/;
    return item.url.match(idRegExp)[1];
  }

  _transformPlanet = (planet) => {
    return {
      ID: this._extractID(planet),
      Name: planet.name,
      Population: planet.population,
      Climate: planet.climate,
      Terrain: planet.terrain,
      "Surface Water, [ % ]": planet.surface_water,
      Gravity: planet.gravity,
      "Diameter, [ km ]": planet.diameter,
      "Rotation Period, [ days ]": planet.rotation_period,
      "Orbital Period, [ days ]": planet.orbital_period
    };
  };

  _transformPerson = (person) => {
    return {
      ID: this._extractID(person),
      Name: person.name,
      Gender: person.gender,
      "Birth Year": person.birth_year,
      "Eye Color": person.eye_color,
      "Hair Color": person.hair_color,
      "Height, [ cm ]": person.height,
      "Mass, [ kg ]": person.mass,
      "Skin Color": person.skin_color,
    };
  };

  _transformStarship = (starship) => {
    return {
      ID: this._extractID(starship),
      Name: starship.name,
      Model: starship.model,
      Manufacturer: starship.manufacturer,
      "Starship Class": starship.starship_class,
      Crew: starship.crew,
      Passengers: starship.passengers,
      "Max Atmosphering Speed, [ km/hour ]": starship.max_atmosphering_speed,
      "Cargo Capacity, [ ton ]": starship.cargo_capacity,
      "Hyperdrive Rating": starship.hyperdrive_rating,
      "Length, [ m ]": starship.length,
      Consumables: starship.consumables,
      MGLT: starship.MGLT,
      "Cost, [ credits ]": starship.cost_in_credits
    }
  };

  _transformVehicle = (vehicle) => {
    return {
      ID: this._extractID(vehicle),
      Name: vehicle.name,
      Model: vehicle.model,
      Manufacturer: vehicle.manufacturer,
      "Vehicle Class": vehicle.vehicle_class,
      Crew: vehicle.crew,
      Passengers: vehicle.passengers,
      "Max Atmosphering Speed, [ km/hour ]": vehicle.max_atmosphering_speed,
      "Cargo Capacity, [ ton ]": vehicle.cargo_capacity,
      Length: vehicle.length,
      Consumables: vehicle.consumables,
      "Cost, [ credits ]": vehicle.cost_in_credits
    }
  };
}

const swapi = new SwapiService();

swapi.getAllVehicles(4);
