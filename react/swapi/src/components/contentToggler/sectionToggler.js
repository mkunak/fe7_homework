import React from 'react';

import styles from "./contentToggler.module.scss";


function SectionToggler({toggleSection}) {
  
  const handleClick = (event) => {
    toggleSection(event.target.id);
  };
  
  return (
    <div className={styles.positionAbsLeft}>
      <div className="btn-group-vertical" data-toggle="buttons">
        <p className="btn btn-warning m-0 text-dark">Toggle Section</p>
        <button type="button"
                id="randomItem"
                onClick={handleClick}
                className="btn btn-outline-warning"
        >Random Item
        </button>
        <button type="button"
                id="itemsList"
                onClick={handleClick}
                className="btn btn-outline-warning"
        >Items List
        </button>
        <button type="button"
                id="itemDetails"
                onClick={handleClick}
                className="btn btn-outline-warning"
        >Item Details
        </button>
      </div>
    </div>
  );
}

export default SectionToggler;
