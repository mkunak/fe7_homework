import React from 'react';
import uuid from 'react-uuid';
import './detailsList.module.scss';


function DetailsList({details}) {
  
  // const {ID: id, Name: name} = details;
  
  let listItems = [];
  for (let key in details) {
    if (key !== 'ID' && key !== 'Name') {
      listItems.push(
        <li key={uuid()}
            className="list-group-item py-1">
          <span>{key}: </span>
          <span className="font-weight-bold">{details[key]}</span>
        </li>
      );
    }
  }
  
  return (
    <ul className="list-group list-group-flush">
      {listItems}
    </ul>
  );
}

export default DetailsList;
