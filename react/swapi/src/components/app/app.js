import React from "react";
import {BrowserRouter, Route} from "react-router-dom";

import Header from "../header";
import SectionToggler from "../contentToggler";
import Footer from "../footer";
import ErrorLoader from "../errorLoader";
import PeoplePage from "../../pages/people";
import PlanetsPage from "../../pages/planets";
import StarshipsPage from "../../pages/starships";
import VehiclesPage from "../../pages/vehicles";
import ItemDetails from "../itemDetails";
import SwapiService from "../../services/swapiService";

import styles from "./app.module.scss";

export default class App extends React.Component {
  swapiService = new SwapiService();
  
  state = {
    showRandomItem: true,
    showItemsList: true,
    showItemDetails: true,
    hasError: false
  };
  
  handleTogglerId = id => {
    if (id === "randomItem") {
      return this.setState(state => {
        return {
          showRandomItem: !state.showRandomItem
        };
      });
    }
    if (id === "itemsList") {
      return this.setState(state => {
        return {
          showItemsList: !state.showItemsList
        };
      });
    }
    if (id === "itemDetails") {
      return this.setState(state => {
        return {
          showItemDetails: !state.showItemDetails
        };
      });
    }
  };
  
  componentDidCatch(error, errorInfo) {
    console.log("Errors have come");
    this.setState({
      hasError: true
    });
  }
  
  render() {
    if (this.state.hasError) {
      return <ErrorLoader/>;
    }
    
    const {showRandomItem, showItemsList, showItemDetails} = this.state;
    
    return (
      <BrowserRouter>
        <div className="container">
          <Header/>
          <SectionToggler toggleSection={this.handleTogglerId}/>
          {/*<HomePage/>*/}
          <Route
            path="/"
            exact
            render={() => (
              <h2 className={`${styles.pageHeight} text-center text-warning`}>
                Wellcome to Star Wars API
              </h2>
            )}
          />
          
          <Route
            path="/people/:id?"
            exact
            render={() => (
              <PeoplePage
                showRandomItem={showRandomItem}
                showItemsList={showItemsList}
                showItemDetails={showItemDetails}
              />
            )}
          />
          
          <Route
            path="/planets"
            exact
            render={() => (
              <PlanetsPage
                showRandomItem={showRandomItem}
                showItemsList={showItemsList}
                showItemDetails={showItemDetails}
              />
            )}
          />
          
          <Route
            path="/planets/:id"
            render={e => {
              console.log(e);
              return (
                <div className={`${styles.pageHeight}`}>
                  <ItemDetails
                    url={"planets/"}
                    itemId={e.match.params.id}
                    getItemDetails={this.swapiService.getPlanet}
                  />
                </div>
              );
            }}
          />
          
          <Route
            path="/starships"
            exact
            render={() => (
              <StarshipsPage
                showRandomItem={showRandomItem}
                showItemsList={showItemsList}
                showItemDetails={showItemDetails}
              />
            )}
          />
          
          <Route
            path="/starships/:id"
            render={e => {
              console.log(e);
              return (
                <div className={`${styles.pageHeight}`}>
                  <ItemDetails
                    url={"starships/"}
                    itemId={e.match.params.id}
                    getItemDetails={this.swapiService.getStarship}
                  />
                </div>
              );
            }}
          />
          
          <Route
            path="/vehicles"
            exact
            render={() => (
              <VehiclesPage
                showRandomItem={showRandomItem}
                showItemsList={showItemsList}
                showItemDetails={showItemDetails}
              />
            )}
          />
          
          <Route
            path="/vehicles/:id"
            render={e => {
              console.log(e);
              return (
                <div className={`${styles.pageHeight}`}>
                  <ItemDetails
                    url={"vehicles/"}
                    itemId={e.match.params.id}
                    getItemDetails={this.swapiService.getVehicle}
                  />
                </div>
              );
            }}
          />
          <Footer/>
        </div>
      </BrowserRouter>
    );
  }
}
