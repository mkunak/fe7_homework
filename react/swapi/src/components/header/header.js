import React from 'react';
import {Link} from 'react-router-dom';

// import styles from './header.module.scss';


const Header = () => {
  return (
    <header className="header py-4">
      <div className="container">
        <div className="border-dark rounded overflow-hidden">
          <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <Link className="navbar-brand h4 mx-4" to="/">[ StarWarsAPI ]</Link>
            <div className="collapse navbar-collapse " id="navbarColor01">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item mx-1">
                  <Link className="nav-link h5 font-weight-bold" to="/people">People</Link>
                </li>
                <li className="nav-item mx-1">
                  <Link className="nav-link h5 font-weight-bold" to="/planets">Planets</Link>
                </li>
                <li className="nav-item mx-1">
                  <Link className="nav-link h5 font-weight-bold" to="/starships">Starships</Link>
                </li>
                <li className="nav-item mx-1">
                  <Link className="nav-link h5 font-weight-bold" to="/vehicles">Vehicles</Link>
                </li>
              </ul>
              <form className="form-inline my-2 my-lg-0">
                <input className="form-control mr-sm-2" type="text" placeholder="Search"/>
                <button className="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
              </form>
            </div>
          </nav>
          <div>
            <ol className="breadcrumb rounded-0 m-0">
              <li className="breadcrumb-item"><Link to="#">Home</Link></li>
              <li className="breadcrumb-item"><Link to="#">Library</Link></li>
              <li className="breadcrumb-item active">Data</li>
            </ol>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
