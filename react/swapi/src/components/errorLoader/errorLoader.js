import React from 'react';

import './errorLoader.scss';
import './errorLoader.scss';

const ErrorLoader = () => {
  return (
    <div className="lds-css ng-scope">
      <div className="lds-gear">
        <div>
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
        </div>
      </div>
      <span className="errorText">something went wrong as we expected</span>
      <span className="errorText">don't worry! we have already sent wizards to help</span>
    </div>
  );
};

export default ErrorLoader;