import React from 'react';
import {Link} from "react-router-dom";

import './footer.module.scss';

const Footer = () => {
  return (
    <footer className="footer py-4">
      <div className="container">
        <div className="border-dark rounded overflow-hidden">
          <nav className="navbar navbar-expand-lg navbar-dark bg-primary d-flex justify-content-end">
            <Link className="navbar-brand h4 mx-4" to="#">[ StarWarsAPI ]</Link>
          </nav>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
