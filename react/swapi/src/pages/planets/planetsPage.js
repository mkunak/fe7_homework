import React from 'react';

import ErrorLoader from "../../components/errorLoader"; // import RandomItem from "../randomItem";
import ItemsList from "../../components/itemsList";
import SwapiService from "../../services/swapiService";
import ItemDetails from "../../components/itemDetails";
import {withRouter} from 'react-router-dom';
import RandomItem from "../../components/randomItem";

import './planetsPage.module.scss';
import styles from "./planetsPage.module.scss";


class PlanetsPage extends React.Component {

  swapiService = new SwapiService();

  itemsListArray = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21];

  state = {
    itemId: Math.floor(Math.random() * (19 - 3)) + 3,
    hasError: false
  };

  componentWillUnmount() {
    this.setState(() => {
      return {
        itemId: null
      };
    });
  }

  componentDidCatch(error, errorInfo) {
    console.log('Errors have come');
    this.setState({
      hasError: true
    })
  }

  setSelectedId = (id) => {
    this.setState({
      itemId: id
    })
  };

  render() {

    if (this.state.hasError) {
      return <ErrorLoader/>;
    }

    const {showRandomItem, showItemsList, showItemDetails, history} = this.props;

    const randomItem = showRandomItem ?
      <RandomItem url={'planets/'}
                  itemsListArray={this.itemsListArray}
                  getItem={this.swapiService.getPlanet}/> : null;

    const itemsList = showItemsList ?
      <ItemsList url={'planets/'}
                 // onItemSelected={this.setSelectedId}
                 onItemSelected={(itemId) => {
                   const newPath = `planets/${itemId}`;
                   history.push(newPath);
                 }}
                 getAllItems={this.swapiService.getAllPlanets}/> : null;

    const itemDetails = showItemDetails ?
      <ItemDetails url={'planets/'}
                   itemId={this.state.itemId}
                   getItemDetails={this.swapiService.getPlanet}/> : null;

    return (
      <section className={`${styles.pageHeight} planets`}>
        <div className="container">
          <div className="row my-4">
            <div className="col-12">
              {randomItem}
            </div>
            <div className="col-md-6">
              {itemsList}
            </div>
            <div className="col-md-6">
              {itemDetails}
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default withRouter(PlanetsPage);
