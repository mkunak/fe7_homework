import React from 'react';

class MyButton extends React.Component {
    state = {
        counter: 0,
        text: '',
    };

    componentDidMount() {
        console.warn('did mount')
    }

    static getDerivedStateFromProps(props) {
        return {
            counter: props.defaultValue,
        }
    }

    handleClick = () => {
        this.setState({
            counter: this.state.counter + 1,
        });
    };

    render() {
        const { counter, text } = this.state;
        const { buttonTitle, valueName } = this.props;

        return (
            <div>
                <button onClick={this.handleClick}>{buttonTitle}</button>
                <p>
                    Current value: {counter} {valueName}
                </p>
                <div>
                    {text}
                </div>
            </div>
        );
    }
};

export default MyButton;