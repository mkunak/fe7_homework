import React from 'react';

import MyButton from "./MyButton";

const App = function() {
  return (
      <>
          <MyButton buttonTitle="click me" valueName="click" defaultValue={5} />

          <MyButton buttonTitle="click this super button"  />
      </>
  );
};

export default App;
