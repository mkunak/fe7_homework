import React from 'react';
import { connect } from 'react-redux';

import { logout, doLogin } from '../../store/actions';


class Login extends React.Component {
  handleLoginButton = () => {
    const { doLogin } = this.props;

    doLogin();
  }

  render() {
    const { logout, user, error } = this.props;

    return (
      <div>
        {error &&(
          <div>
            ERROR!!! <br />
            {error.message}
          </div>
        )}
        <p>{user ? user.name : 'no user' }</p>
        <button onClick={logout}>
          Logout
        </button>
        <button onClick={this.handleLoginButton}>
          Login
        </button>
      </div>
    );
  }
};

const mapStateToProps = function(state) {
  return {
    user: state.user,
    error: state.error,
  };
};

const mapDispatchToProps = {
  logout,
  doLogin,
};

const enhancer = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default enhancer(Login);
