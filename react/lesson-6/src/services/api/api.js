const baseUrl = 'https://myapi.com';

export default {
  /*
    GET: /login

    RESPONSE:
      {
        name: 'Vasya',
        age: 16,
        userImage: 'https://lorempixel.com/100/100',
      }
  */

  loginUser() {
    return Promise.resolve({
      name: 'Vasya',
      age: 16,
      userImage: 'https://lorempixel.com/100/100',
    });


    // return fetch(`${baseUrl}/login`)
    //   .then(response => response.json());
  },
};