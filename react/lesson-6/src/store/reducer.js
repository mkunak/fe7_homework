import { LOGIN, LOGOUT, PROMOTION, ERROR } from './actionNames';

export default function(state, action) {
  switch (action.type) {
    case LOGIN:
      return {
        user: action.payload,
      };

    case LOGOUT:
      return {
        user: null,
      };

    case ERROR:
      return {
        ...state,
        isError: true,
        error: action.payload,
      };

    case PROMOTION:
      return {
        user: {
          ...state.user,
          isAdmin: false,
        },
      };

    default:
      return state;
  }
};