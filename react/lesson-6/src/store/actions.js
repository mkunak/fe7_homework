import api from '../services/api';

import { LOGIN, LOGOUT, PROMOTION, ERROR } from './actionNames';


export function login(payload) {
  return {
    type: LOGIN,
    payload,
  };
}

function errorLogin(payload) {
  return {
    type: ERROR,
    payload,
  };
}

export function doLogin(payload) {
  return function(dispatch) {
    api.loginUser().then((data) => {
      if (Math.random() > 0.5) {
        throw new Error('Something went wrong! Error code: 503');
      }

      dispatch(login(data));
    }).catch((error) => {
      dispatch(errorLogin(error))
    });
  }
}

export function error(payload) {
  return {
    type: LOGIN,
    payload,
  };
}

export function logout() {
  return {
    type: LOGOUT,
  };
}

export function promotion() {
  return {
    type: PROMOTION,
  };
}
