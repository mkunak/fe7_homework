import { createStore, compose, applyMiddleware } from 'redux';

import reducer from './reducer';
import logger from './middlewares/logger';
import retryLogin from './middlewares/retryLogin';
import thunk from 'redux-thunk';

const initialValue = {
  user: null,
};

const enhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducer,
  initialValue,
  enhancer(applyMiddleware(thunk, logger, retryLogin)),
);

export default store;
