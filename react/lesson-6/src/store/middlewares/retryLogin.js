import { doLogin } from '../actions';

const retryLogin = function(store) {
  return function(next) {
    return function(action) {
      action.date = new Date();

      if (action.type === 'ERROR') {
        store.dispatch(doLogin());
      }

      next(action);
    };
  };
};

export default retryLogin;
